# Confluence Roadmap plugin

Simple roadmap tool for Confluence

## Building and Releasing

Releasing this plugin is managed by a Bamboo deployment project.

The Bamboo build for this plugin is located at https://collaboration-bamboo.internal.atlassian.com/browse/CBP-CONFROADMAPTEST

## Ownership

This plugin is owned by the Solution(Doraemon) team. Go to "Doraemon" Hipchat channel if you have questions.
