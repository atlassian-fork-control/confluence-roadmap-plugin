package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.PageComponent;
import com.atlassian.confluence.webdriver.pageobjects.component.dialog.MacroForm;
import com.atlassian.confluence.webdriver.pageobjects.component.editor.MacroPropertyPanel;
import com.atlassian.pageobjects.PageBinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class RoadmapMacroPropertyPanel extends MacroPropertyPanel implements PageComponent
{
    @Inject
    private PageBinder binder;

    public MacroForm edit()
    {
        propertyPanelElement.find(By.className("macro-placeholder-property-panel-edit-button")).click();
        return binder.bind(MacroForm.class);
    }
}
