package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;

public class LaneRenameDialog extends Dialog
{
    @ElementBy(cssSelector = ".lane-title")
    private PageElement laneTitleText;

    @ElementBy(cssSelector = ".rename-button")
    private PageElement saveButton;

    public LaneRenameDialog()
    {
        super("inline-dialog-roadmap-dialog");
    }

    public LaneRenameDialog clickSave()
    {
        saveButton.click();
        return this;
    }

    public LaneRenameDialog inputTextTitle(String title)
    {
        waitUntilTrue(laneTitleText.timed().isVisible());
        laneTitleText.clear();
        laneTitleText.type(title);
        waitUntil(laneTitleText.timed().getValue(), is(title));
        return this;
    }

}
