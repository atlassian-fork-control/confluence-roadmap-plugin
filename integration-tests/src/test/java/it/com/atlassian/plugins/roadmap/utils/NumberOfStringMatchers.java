package it.com.atlassian.plugins.roadmap.utils;

import java.util.regex.Pattern;
import org.hamcrest.*;

/**
 * Matcher which tests if a string contains a specified number of substrings.
 *
 */
public class NumberOfStringMatchers extends TypeSafeMatcher<String>
{

    private final String substring;
    private final int count;

    public NumberOfStringMatchers(String substring, int count)
    {
        this.substring = substring;
        this.count = count;
    }

    @Override
    protected boolean matchesSafely(String item)
    {
        Pattern p = Pattern.compile(substring);
        java.util.regex.Matcher m = p.matcher(item);
        int actualCount = 0;
        while (m.find())
        {
            actualCount++;
        }
        if (actualCount == count)
            return true;
        else
            return false;
    }

    public void describeTo(Description description)
    {
        description.appendText("String containing ").appendText(count + " occurences of ").appendText(substring);

    }

    @Override
    public void describeMismatchSafely(String item, Description mismatchDescription)
    {
        mismatchDescription.appendValue(count).appendText(" occurence(s) of ").appendValue(substring).appendText(" in ").appendText(item);
    }

    @Factory
    public static Matcher<String> hasNumberOfSubstrings(String substring, int count)
    {
        return new NumberOfStringMatchers(substring, count);
    }

}
