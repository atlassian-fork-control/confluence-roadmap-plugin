package it.com.atlassian.plugins.roadmap.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.dialog.Dialog;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import it.com.atlassian.plugins.roadmap.utils.RoadmapStringUtils;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Map;

public class LaneDialog extends Dialog
{
    @ElementBy(id = "confluence-roadmap-dialog")
    private PageElement roadmapEditorDialog;

    @ElementBy(id = "delete-button")
    private PageElement deleteButton;

    @ElementBy(id = "add-bar-button")
    private PageElement addBarButton;
    
    @ElementBy(id = "rename-button")
    private PageElement renameButton;

    @ElementBy(id = "lane-color-select")
    private PageElement laneColorSelect;

    @ElementBy(id = "lane-color-picker")
    private PageElement laneColorPicker;

    public LaneDialog()
    {
        super("inline-dialog-roadmap-dialog");
    }

    public LaneDialog openForFirstLane()
    {
        PageElement firstLane = roadmapEditorDialog.find(By.cssSelector(".roadmap-lane:first-child > .roadmap-lane-title"));
        firstLane.click();
        return this;
    }

    public LaneDialog open(String laneTitle)
    {
        PageElement laneElement = null;
        List<PageElement> laneElements = roadmapEditorDialog.findAll(By.cssSelector(".roadmap-lane-title"));
        for (PageElement currentLane : laneElements)
        {
            String title = currentLane.find(By.cssSelector(".title-inner")).getAttribute("innerHTML"); //trick to get text which has been rotated
            if (title.equals(laneTitle))
            {
                laneElement = currentLane;
                break;
            }
        }
        laneElement.click();
        return this;
    }

    public LaneRenameDialog clickRename()
    {
        renameButton.click();
        return this.pageBinder.bind(LaneRenameDialog.class);
    }

    public LaneDialog clickDelete() {
        deleteButton.click();
        return this;
    }

    public LaneDialog clickAddBar()
    {
        addBarButton.click();
        return this;
    }

    public LaneDialog changeColor(String newColor)
    {
        laneColorSelect.click(); // click to show list colors
        PageElement laneColorElement = null;
        List<PageElement> laneColorElements = laneColorPicker.findAll(By.cssSelector(".color-item"));
        for (PageElement currentLaneColorElement : laneColorElements)
        {
            Map<String, String> styleParameters =
                    RoadmapStringUtils.getParameters(currentLaneColorElement.getAttribute("style"), ";", ":");
            String color = styleParameters.get("background-color");
            if (color.equals(newColor))
            {
                laneColorElement = currentLaneColorElement;
                break;
            }
        }
        laneColorElement.click();
        return this;
    }

    public TimedQuery<Boolean> isDeleteButtonPresent()
    {
        return deleteButton.timed().isPresent();
    }
}
