// Karma configuration

module.exports = function(config) {
    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: '',

        // frameworks to use
        frameworks: ['qunit', 'sinon'],

        // list of files / patterns to load in the browser
        files: [
            'target/qunit/dependencies/jquery-min.js',
            'target/qunit/dependencies/js-vendor/jquery/jquery-ui/jquery-ui-min.js',
            'target/qunit/dependencies/js-vendor/underscorejs/underscore-min.js',
            'target/qunit/dependencies/js-vendor/backbone/backbone-min.js',

            {pattern: 'target/qunit/dependencies/**/*.js', included: false},
            {pattern: 'target/qunit/soy/*.soy.js', included: true},


            /* Some libs which need to run tests but they are not in AUI dependencies */
            {pattern: 'src/test/resources/lib/*.js'},

            {pattern: 'src/test/resources/mock/*.js'},

            /* Production code to test.
             * TODO: Because our production code does not have module dependencies management (like AMD),
             * so we need to put explicitly them here.
             * */
            'src/main/resources/editor/roadmap-editor-config.js',
            'src/main/resources/utils/roadmap-helper.js',
            'src/main/resources/utils/roadmap-utilities.js',

            /* Test files */
            {pattern: 'src/test/resources/qunit/*-test.js', included: true}
        ],

        // list of files to exclude
        exclude: [
        ],

        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['progress', 'junit', 'coverage'],
        junitReporter: {
            outputFile: 'target/surefire-reports/npm-test-results.xml',
            suite: ''
        },

        // configuration for Istanbul code coverage tool
        preprocessors: {
            'src/main/resources/**/*.js': 'coverage'
        },

        coverageReporter: {
            type: 'lcov',
            dir: 'target/coverage/'
        },

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        browsers: ['ChromeHeadless'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false
    });
};