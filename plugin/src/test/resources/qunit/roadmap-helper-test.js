(function() {
    /* global QUnit:false */
    'use strict';

    QUnit.module('Roadmap utilities', {
        beforeEach: function() {
            this.oldWeekWidth = window.Roadmap ? window.Roadmap.WEEK_WIDTH : undefined;

            window.Roadmap = window.Roadmap || {};
            window.Roadmap.WEEK_WIDTH = 70;
        },

        afterEach: function() {
            window.Roadmap.WEEK_WIDTH = this.oldWeekWidth;
        }
    });

    QUnit.test('test "getPosXOnWeekTimeline" method', function() {
        var getPosXOnWeekTimeline = Confluence.Roadmap.Helper.getPosXOnWeekTimeline;

        // start day is after timeline start day.
        var timeline = {startDate: new Date(2015, 4 - 1, 6)};
        var startDate = new Date(2015, 3, 20);
        var pos = getPosXOnWeekTimeline(timeline, startDate);
        QUnit.assert.equal(pos, window.Roadmap.WEEK_WIDTH * 2);

        // timeline start day is not Monday.
        var timeline = {startDate: new Date(2015, 4 - 1, 6)};
        var startDate = new Date(2015, 3, 20);
        var pos = getPosXOnWeekTimeline(timeline, startDate);
        QUnit.assert.equal(pos, window.Roadmap.WEEK_WIDTH * 2);

        // start day is before timeline start day.
        // Monday.
        timeline = {startDate: new Date(2015, 4 - 1, 6)};
        startDate = new Date(2015, 4 - 1, 3);
        pos = getPosXOnWeekTimeline(timeline, startDate);
        QUnit.assert.equal(pos, -(3 * window.Roadmap.WEEK_WIDTH / 7));
    });

    QUnit.test('test "getWeekStartDateByPosition" method', function() {
        var getWeekStartDateByPosition = Confluence.Roadmap.Helper.getWeekStartDateByPosition;

        var timeline = {startDate: new Date(2015, 4 - 1, 6)};
        var pos = 140; // 2 weeks
        var date = getWeekStartDateByPosition(timeline, pos);
        QUnit.assert.ok(moment('2015-04-20', 'YYYY-MM-DD').diff(date) === 0);

        timeline = {startDate: new Date(2015, 4 - 1, 6)};
        pos = 150; // > 2 weeks
        date = getWeekStartDateByPosition(timeline, pos);
        QUnit.assert.ok(moment('2015-04-21', 'YYYY-MM-DD').diff(date) === 0);

        timeline = {startDate: new Date(2015, 4 - 1, 6)};
        pos = 160; // > 2 weeks
        date = getWeekStartDateByPosition(timeline, pos);
        QUnit.assert.ok(moment('2015-04-22', 'YYYY-MM-DD').diff(date) === 0);

        timeline = {startDate: new Date(2015, 4 - 1, 6)};
        pos = -50; // > 2 weeks
        date = getWeekStartDateByPosition(timeline, pos);
        QUnit.assert.ok(moment('2015-04-01', 'YYYY-MM-DD').diff(date) === 0);
    });

}());
