package com.atlassian.plugins.roadmap.renderer;

import org.apache.batik.ext.awt.g2d.GraphicContext;
import org.apache.batik.svggen.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.geom.AffineTransform;

/**
 * Customized class to be able to get last added element to the SVG DOM tree
 */
public class ConfluenceSVGGraphics2D extends SVGGraphics2D
{
    protected Element lastAddedElement;
    private Element rootElement;

    public ConfluenceSVGGraphics2D(Document document)
    {
        super(document);
    }

    public ConfluenceSVGGraphics2D(Document document, ImageHandler imageHandler, ExtensionHandler extensionHandler, boolean b)
    {
        super(document, imageHandler, extensionHandler, b);
    }

    public ConfluenceSVGGraphics2D(SVGGeneratorContext svgGeneratorContext, boolean b)
    {
        super(svgGeneratorContext, b);
    }

    /**
     * Override to use the customized DOMGroupManagerEx
     * @param svgGraphics2D svgGraphics2D
     */
    public ConfluenceSVGGraphics2D(SVGGraphics2D svgGraphics2D)
    {
        super(svgGraphics2D);
        this.domTreeManager.removeGroupManager(this.domGroupManager);
        this.domGroupManager = new DOMGroupManagerEx(this.gc, this.domTreeManager);
        this.domTreeManager.addGroupManager(this.domGroupManager);
    }

    /**
     * Override to use the customized DOMGroupManagerEx
     */
    @Override
    protected void setGeneratorContext(SVGGeneratorContext generatorCtx)
    {
        this.generatorCtx = generatorCtx;

        this.gc = new GraphicContext(new AffineTransform());

        SVGGeneratorContext.GraphicContextDefaults gcDefaults =
                generatorCtx.getGraphicContextDefaults();

        if (gcDefaults != null)
        {
            if (gcDefaults.getPaint() != null)
            {
                gc.setPaint(gcDefaults.getPaint());
            }
            if (gcDefaults.getStroke() != null)
            {
                gc.setStroke(gcDefaults.getStroke());
            }
            if (gcDefaults.getComposite() != null)
            {
                gc.setComposite(gcDefaults.getComposite());
            }
            if (gcDefaults.getClip() != null)
            {
                gc.setClip(gcDefaults.getClip());
            }
            if (gcDefaults.getRenderingHints() != null)
            {
                gc.setRenderingHints(gcDefaults.getRenderingHints());
            }
            if (gcDefaults.getFont() != null)
            {
                gc.setFont(gcDefaults.getFont());
            }
            if (gcDefaults.getBackground() != null)
            {
                gc.setBackground(gcDefaults.getBackground());
            }
        }

        this.shapeConverter = new SVGShape(generatorCtx);
        this.domTreeManager = new DOMTreeManager(gc,
                generatorCtx,
                DEFAULT_MAX_GC_OVERRIDES);
        this.domGroupManager = new DOMGroupManagerEx(gc, domTreeManager);
        this.domTreeManager.addGroupManager(domGroupManager);
        this.setDOMTreeManager(this.domTreeManager);
    }


    /**
     * If we call getRoot() multiple time the result will be different so we
     * need to cache here
     */
    @Override
    public Element getRoot()
    {
        if (rootElement == null)
        {
            rootElement = super.getRoot();
        }

        return rootElement;
    }


    /**
     * Gets the last added element to the SVG DOM (as the result of drawing to the Graphic
     *
     * @return The last added element
     */
    public Element getLastAddedElement()
    {
        return lastAddedElement;
    }

    public class DOMGroupManagerEx extends DOMGroupManager
    {
        public DOMGroupManagerEx(GraphicContext graphicContext, DOMTreeManager domTreeManager)
        {
            super(graphicContext, domTreeManager);
        }

        @Override
        public void addElement(Element element, short i)
        {
            ConfluenceSVGGraphics2D.this.lastAddedElement = element;
            super.addElement(element, i);
        }
    }

}
