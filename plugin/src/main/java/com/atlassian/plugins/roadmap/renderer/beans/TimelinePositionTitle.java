package com.atlassian.plugins.roadmap.renderer.beans;

/**
 *
 */
public class TimelinePositionTitle
{
    private String month;
    private String year;

    public TimelinePositionTitle()
    {
        //default constructor
    }

    public TimelinePositionTitle(String month, String year)
    {
        this.month = month;
        this.year = year;
    }

    public String getMonth()
    {
        return month;
    }

    public void setMonth(String month)
    {
        this.month = month;
    }

    public String getYear()
    {
        return year;
    }

    public void setYear(String year)
    {
        this.year = year;
    }
}
