package com.atlassian.plugins.roadmap.models;

/**
 *
 */
public class LaneColor
{
    private String lane;
    private String bar;
    private String text;

    public LaneColor()
    {
        // Default constructor stub
    }
    
    public LaneColor(String lane, String bar, String text)
    {
        this.lane = lane;
        this.bar = bar;
        this.text = text;
    }
    
    public String getLane()
    {
        return lane;
    }

    public void setLane(String lane)
    {
        this.lane = lane;
    }

    public String getBar()
    {
        return bar;
    }

    public void setBar(String bar)
    {
        this.bar = bar;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }
}
