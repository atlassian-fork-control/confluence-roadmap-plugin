package com.atlassian.plugins.roadmap;

import com.atlassian.confluence.api.service.exceptions.NotFoundException;
import com.atlassian.confluence.api.service.exceptions.PermissionException;
import com.atlassian.confluence.api.service.exceptions.ServiceException;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLink;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.ResourceIdentifier;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.xhtml.api.Link;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.MacroDefinitionUpdater;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugins.roadmap.models.Bar;
import com.atlassian.plugins.roadmap.models.Lane;
import com.atlassian.plugins.roadmap.models.RoadmapPageLink;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.renderer.RenderContext;
import com.atlassian.vcache.PutPolicy;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.vcache.VCacheUtils.join;

//@Component
public class TimelinePlannerMacroManager
{
    private static final String ROADMAP_MACRO_NAME = "roadmap";
    private static final String PAGELINK_CACHE_PREFIX = "pagelink-";
    public enum LinkStatus
    {
        PENDING, REDEEM, UNKNOWN
    }

    private PageManager pageManager;
    private CommentManager commentManager;
    private XhtmlContent xhtmlContent;
    private PermissionManager permissionManager;
    private final PageLinkParser pageLinkParser;
    private final ContentEntityManager contentEntityManager;
    private final RoadmapMacroCacheSupplier cacheSupplier;

//    @Autowired
    public TimelinePlannerMacroManager(XhtmlContent xhtmlContent,
                                       PageManager pageManager,
                                       CommentManager commentManager,
                                       PageLinkParser pageLinkParser,
                                       PermissionManager permissionManager,
                                       ContentEntityManager contentEntityManager,
                                       RoadmapMacroCacheSupplier cacheSupplier)
    {
        this.pageManager = pageManager;
        this.commentManager = commentManager;
        this.xhtmlContent = xhtmlContent;
        this.permissionManager = permissionManager;
        this.pageLinkParser = pageLinkParser;
        this.contentEntityManager = contentEntityManager;
        this.cacheSupplier = cacheSupplier;
    }

    public void updatePagelinkToRoadmapBar(final BarParam barParam, final long linkPageId)
    {
        RoadmapPageLink pageLink = new RoadmapPageLink(pageManager.getAbstractPage(linkPageId));
        updatePagelinkToRoadmapBar(barParam, pageLink);
    }

    public void updatePagelinkToRoadmapBar(final BarParam barParam, final RoadmapPageLink linkPage)
    {
        checkUpdatePagePermission(barParam.contentId);

        if ((linkPage.getId() != null) || (linkPage.getId() == null && !barParam.updateRoadmap))
        {
            putBarPageLink(barParam.barId, linkPage);
        }
        else
        {
            removeBarPageLink(barParam.barId);
        }

        if (barParam.updateRoadmap)
        {
            ContentEntityObject content = getLatestVersionContent(barParam.contentId);
            final PageContext pageContext = content.toPageContext();
            String updateContent = null;
            try
            {
                updateContent = xhtmlContent.updateMacroDefinitions(content.getBodyAsString(),
                        new DefaultConversionContext(pageContext), new MacroDefinitionUpdater()
                        {
                            @Override
                            public MacroDefinition update(MacroDefinition macroDefinition)
                            {
                                final Map<String, String> params = macroDefinition.getParameters();
                                if (macroDefinition.getName().equals(ROADMAP_MACRO_NAME) && params.get("hash").equals(
                                        barParam.roadmapHash))
                                {
                                    TimelinePlanner roadmap = TimelinePlannerJsonBuilder.fromJson(
                                            macroDefinition.getParameter(
                                                    "source"));
                                    Bar bar = getBarInRoadmap(roadmap, barParam.barId);
                                    bar.setPageLink(linkPage);
                                    macroDefinition.setTypedParameter("source", TimelinePlannerJsonBuilder.toJson(
                                            roadmap));

                                    // Update mapped links
                                    String maplinks = StringUtils.defaultString(macroDefinition.getTypedParameter(
                                            "maplinks",
                                            String.class));
                                    List<Link> pagelinks = extractLinksFromMacroParam(macroDefinition.getTypedParameter(
                                            "pagelinks",
                                            Object.class));

                                    String wikiLink = StringEscapeUtils.escapeHtml4(linkPage.getWikiLink());
                                    ResourceIdentifier resourceIdentifier = pageLinkParser.parse(
                                            StringUtils.substringBetween(wikiLink, "[", "]"), pageContext.getSpaceKey());

                                    if (!StringUtils.contains(maplinks, barParam.barId) && resourceIdentifier != null)
                                    {
                                        maplinks = StringUtils.isEmpty(maplinks) ? barParam.barId :
                                                maplinks + RoadmapMacro.LINKS_DELIMITER + barParam.barId;
                                        pagelinks.add(new DefaultLink(resourceIdentifier, null));
                                    }
                                    else
                                    {
                                        List<String> maplinksList = Lists.newArrayList(maplinks.split(
                                                RoadmapMacro.LINKS_DELIMITER));
                                        int linkIndex = maplinksList.indexOf(barParam.barId);
                                        if (resourceIdentifier != null)  // user link to another page
                                        {
                                            Link newLink = pagelinks.get(linkIndex).updateDestination(resourceIdentifier);
                                            pagelinks.set(linkIndex, newLink);
                                        }
                                        else if (linkIndex >= 0)  // user clear linked page
                                        {
                                            maplinksList.remove(linkIndex);
                                            maplinks = StringUtils.join(maplinksList, RoadmapMacro.LINKS_DELIMITER);
                                            pagelinks.remove(linkIndex);
                                        }
                                    }
                                    macroDefinition.setTypedParameter("maplinks", maplinks);
                                    macroDefinition.setTypedParameter("pagelinks", pagelinks);

                                    return macroDefinition;
                                }
                                return macroDefinition;
                            }
                        }
                );
            }
            catch (XhtmlException e)
            {
                throw new ServiceException("Can not update content: ", e);
            }

            if (content instanceof AbstractPage)
            {
                content.setBodyAsString(updateContent);
                pageManager.saveContentEntity(content, DefaultSaveContext.MINOR_EDIT);
            }
            else if (content instanceof Comment)
            {
                commentManager.updateCommentContent((Comment)content, updateContent);
            }
        }
    }

    public MacroDefinition findRoadmapMacroDefinition(final long pageId, final int version, final String roadmapHash) throws XhtmlException
    {
        ContentEntityObject pageContent = getLatestVersionContent(pageId);
        final AtomicReference<MacroDefinition> ref = new AtomicReference<MacroDefinition>();
        xhtmlContent.handleMacroDefinitions(pageContent.getBodyAsString(), new DefaultConversionContext(new RenderContext()), new MacroDefinitionHandler()
        {
            @Override
            public void handle(MacroDefinition macroDefinition)
            {
                final Map<String, String> params = macroDefinition.getParameters();
                if (macroDefinition.getName().equals(ROADMAP_MACRO_NAME) && params.get("hash").equals(roadmapHash))
                {
                    ref.set(macroDefinition);
                }
            }
        });
        return ref.get();
    }

    private Bar getBarInRoadmap(TimelinePlanner timelinePlanner, final String barUUID)
    {
        for(Lane lane: timelinePlanner.getLanes())
        {
            for(Bar bar: lane.getBars())
            {
                if (StringUtils.equals(barUUID, bar.getId()))
                    return bar;
            }
        }
        return null;
    }

    private ContentEntityObject getLatestVersionContent(long contentId)
    {
        ContentEntityObject content = contentEntityManager.getById(contentId);
        if (content == null)
            throw new NotFoundException("No content found with id: " + contentId);

        content = (ContentEntityObject) content.getLatestVersion();

        if (content instanceof Comment)
        {
            return commentManager.getComment(content.getId());
        }
        return content;
    }

    public void put(String barId, LinkStatus linkStatus)
    {
        join(cacheSupplier.getLinkStatusCache().put(barId, linkStatus, PutPolicy.PUT_ALWAYS));
    }

    public LinkStatus checkStatus(String barId)
    {
        return join(cacheSupplier.getLinkStatusCache().get(barId)).orElse(LinkStatus.UNKNOWN);
    }

    public void removeStatus(String barId)
    {
        join(cacheSupplier.getLinkStatusCache().remove(barId));
    }

    public void putBarPageLink(String barId, RoadmapPageLink barPageLink)
    {
        join(cacheSupplier.getPageLinkCache().put(barId, barPageLink, PutPolicy.PUT_ALWAYS));
    }

    public RoadmapPageLink getBarPageLink(String barId)
    {
        return join(cacheSupplier.getPageLinkCache().get(barId)).orElse(new RoadmapPageLink());
    }

    public void removeBarPageLink(String barId)
    {
        join(cacheSupplier.getPageLinkCache().remove(barId));
    }

    private void checkUpdatePagePermission(Long pageId)
    {
        if (pageId != null)
        {
            ContentEntityObject content = contentEntityManager.getById(pageId);
            ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
            if (!permissionManager.hasPermission(currentUser, Permission.EDIT, content))
            {
                throw new PermissionException();
            }
        }
    }

    private List<Link> extractLinksFromMacroParam(Object pagelinksObj)
    {
        if (pagelinksObj instanceof Link)
        {
            return Lists.<Link>newArrayList((Link) pagelinksObj);
        }
        else if (pagelinksObj instanceof List)
        {
            return (List<Link>) pagelinksObj;
        }
        else
        {
            return Lists.<Link>newArrayList();
        }
    }
}