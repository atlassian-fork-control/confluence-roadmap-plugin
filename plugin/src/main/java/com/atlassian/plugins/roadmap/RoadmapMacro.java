package com.atlassian.plugins.roadmap;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.ConfluenceRenderContextOutputType;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.UserAgentUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugins.roadmap.analytics.RoadmapAnalyticObject;
import com.atlassian.plugins.roadmap.models.Bar;
import com.atlassian.plugins.roadmap.models.Lane;
import com.atlassian.plugins.roadmap.models.RoadmapPageLink;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.plugins.roadmap.renderer.PNGRoadMapRenderer;
import com.atlassian.plugins.roadmap.renderer.SVGRoadMapRenderer;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.vcache.PutPolicy;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Set;

import static com.atlassian.vcache.VCacheUtils.unsafeJoin;

/**
 */
public class RoadmapMacro implements Macro, EditorImagePlaceholder
{
    private static final String PLACEHOLDER_SERVLET = "/plugins/servlet/roadmap/image/placeholder";

    private static final int PLACEHOLDER_WIDTH_MAX = 1000;
    private static final int PLACEHOLDER_HEIGHT_MAX = 300;
    private static final String PARAM_RENDER_CONTEXT = "renderContext";
    private static final Set<String> PNG_CONTEXTS = ImmutableSet.of(RenderContextOutputType.EMAIL, RenderContextOutputType.PDF,
                                                    ConfluenceRenderContextOutputType.PAGE_GADGET.toString());
    private static final Set<String> SVG_CONTEXTS = ImmutableSet.of(RenderContextOutputType.DISPLAY, RenderContextOutputType.PREVIEW);
    private static final String PARAM_PAGE_LINK = "pagelinks";
    private static final String PARAM_MAP_LINK = "maplinks";
    private static final String MOBILE_OUTPUT_DEVICE_TYPE = "mobile";
    public static final String LINKS_DELIMITER = "~~~~~";

    private static final String REQUIRED_CONTEXT = "roadmap-view-resources";

    private final RoadmapMacroCacheSupplier cacheSupplier;
    private final I18nResolver i18n;
    private final EventPublisher eventPublisher;
    private final PermissionManager permissionManager;
    private final PageLinkParser pageLinkParser;
    private final PageBuilderService pageBuilderService;

    public RoadmapMacro(I18nResolver i18n, EventPublisher eventPublisher,
            RoadmapMacroCacheSupplier cacheSupplier,
            PermissionManager permissionManager,
            PageLinkParser pageLinkParser,
            PageBuilderService pageBuilderService)
    {
        this.i18n = i18n;
        this.eventPublisher = eventPublisher;
        this.cacheSupplier = cacheSupplier;
        this.permissionManager = permissionManager;
        this.pageLinkParser = pageLinkParser;
        this.pageBuilderService = pageBuilderService;
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException
    {
        pageBuilderService.assembler().resources().requireContext(REQUIRED_CONTEXT);
        try
        {
            if (isTimelinePlanner(parameters))
            {
                return getTimelinePlannerView(parameters, context);
            }
            else
            {
                return getRoadmapView(parameters, context);
            }
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
        catch (IOException ioe)
        {
            throw new RuntimeException(ioe);
        }
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    @Override
    public ImagePlaceholder getImagePlaceholder(Map<String, String> parameters, ConversionContext conversionContext)
    {
        return generateImagePlaceholder(parameters);
    }

    private String getRoadmapView(Map<String, String> parameters, ConversionContext context) throws UnsupportedEncodingException
    {
        Map<String, Object> ctx = MacroUtils.defaultVelocityContext();
        String title = parameters.get("title");
        if (title != null) {
            ctx.put("title", URLDecoder.decode(title, "UTF-8"));
        }
        ctx.put("id", context.getEntity().getId());
        ctx.put("version", context.getEntity().getVersion());
        ctx.put("hash", parameters.get("hash"));
        return VelocityUtils.getRenderedTemplate("templates/view.vm", ctx);
    }

    private String getTimelinePlannerView(Map<String, String> parameters, ConversionContext context)
            throws IOException
    {
        TimelinePlanner roadmap = TimelinePlannerJsonBuilder.fromJson(parameters.get("source"));
        updateLinkedPagesTitle(roadmap, parameters.get(PARAM_PAGE_LINK), parameters.get(PARAM_MAP_LINK), context);
        Map<String, Object> ctx = MacroUtils.defaultVelocityContext();

        ContentEntityObject contentEntity = context.getEntity();
        String outputType = context.getOutputType();
        if (SVG_CONTEXTS.contains(outputType) || PNG_CONTEXTS.contains(outputType))
        {
            ctx.put("id", contentEntity.getId());
            ctx.put("version", contentEntity.getVersion());
            ctx.put("hash", parameters.get("hash"));

            if (SVG_CONTEXTS.contains(outputType) &&
                    !UserAgentUtil.isBrowserMajorVersion(UserAgentUtil.BrowserMajorVersion.MSIE8) &&
                    !MOBILE_OUTPUT_DEVICE_TYPE.equals(context.getOutputDeviceType()))
            {
                SVGRoadMapRenderer svgRoadMapRenderer = new SVGRoadMapRenderer();
                svgRoadMapRenderer.setI18n(i18n);
                String svgXmlCode = svgRoadMapRenderer.renderAsString(roadmap);
                ctx.put("svgHtml", svgXmlCode);
            }
        }
        else // Roadmap image uses Base64
        {
            PNGRoadMapRenderer pngRoadMapRenderer = new PNGRoadMapRenderer();
            pngRoadMapRenderer.setI18n(this.i18n);
            ctx.put("data", pngRoadMapRenderer.renderAsBase64(roadmap));
        }

        setPagePermission(ctx, contentEntity);
        ctx.put(PARAM_RENDER_CONTEXT, outputType);

        if (StringUtils.equals(context.getOutputType(), RenderContextOutputType.DISPLAY))
        {
            eventPublisher.publish(new RoadmapAnalyticObject(roadmap));
        }

        return VelocityUtils.getRenderedTemplate("templates/timeline-planner-view.vm", ctx);
    }

    private boolean isTimelinePlanner(Map<String, String> parameters)
    {
        return Boolean.parseBoolean(parameters.get("timeline"));
    }

    private void setPagePermission(Map<String, Object> context, ContentEntityObject entityObject)
    {
        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
        context.put("canUserEditPage", permissionManager.hasPermission(currentUser, Permission.EDIT, entityObject));
    }

    private void updateLinkedPagesTitle(TimelinePlanner roadmap, String pageLinks, String maplinks, ConversionContext context)
    {
        String[] pageLinkArray = StringUtils.splitByWholeSeparator(pageLinks, LINKS_DELIMITER);
        String[] mapLinkArray = StringUtils.splitByWholeSeparator(maplinks, LINKS_DELIMITER);
        if (pageLinkArray == null || mapLinkArray == null || pageLinkArray.length != mapLinkArray.length)
        {
            return;
        }

        for (Lane lane : roadmap.getLanes())
        {
            for (Bar bar : lane.getBars())
            {
                int barIdIndex = ArrayUtils.indexOf(mapLinkArray, bar.getId());
                if (barIdIndex > -1)
                {
                    String pageTitle = pageLinkArray[barIdIndex];
                    RoadmapPageLink pageLink = pageLinkParser.resolveConfluenceLink(pageTitle,
                            context.getSpaceKey());

                    // To fix the case user revert to previous version which contains linked to pages that have title changed
                    if (pageLink.getId() == null)
                    {
                        pageLink.setId(bar.getPageLink().getId());
                    }

                    bar.setPageLink(pageLink);
                }
            }
        }
    }

    private ImagePlaceholder generateImagePlaceholder(Map<String, String> parameters)
    {
        unsafeJoin(cacheSupplier.getMarcoSourceCache().put(parameters.get("hash"), parameters.get("source"), PutPolicy.PUT_ALWAYS));

        String placeholderUrl = PLACEHOLDER_SERVLET + "?hash=" + parameters.get("hash")
                                + "&width=" + PLACEHOLDER_WIDTH_MAX
                                + "&height=" + PLACEHOLDER_HEIGHT_MAX;

        if (parameters.get("timeline") != null)
        {
            placeholderUrl = placeholderUrl + "&timeline=true";
        }
        return new DefaultImagePlaceholder(placeholderUrl, false, null);
    }
}
