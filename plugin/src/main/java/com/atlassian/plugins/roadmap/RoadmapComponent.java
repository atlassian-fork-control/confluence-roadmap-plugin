package com.atlassian.plugins.roadmap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import static com.atlassian.vcache.VCacheUtils.join;

@SuppressWarnings("unused")
//@Component
public class RoadmapComponent implements InitializingBean, DisposableBean
{
    private final Logger logger = LoggerFactory.getLogger(RoadmapComponent.class);
    private final RoadmapMacroCacheSupplier cacheSupplier;

//    @Autowired
    public RoadmapComponent(RoadmapMacroCacheSupplier cacheSupplier)
    {
        this.cacheSupplier = cacheSupplier;
    }

    /**
     * Called as the plugin is enabled.
     */
    @Override
    public void afterPropertiesSet() throws Exception
    {
        flushCaches();
    }

    /**
     * Called as the plugin is disabled.
     */
    @Override
    public void destroy() throws Exception
    {
        flushCaches();
    }

    private void flushCaches()
    {
        logger.info("Clearing caches [" + RoadmapMacroCacheSupplier.IMAGE_CACHE_NAME + "," + RoadmapMacroCacheSupplier.MACRO_SOURCE_CACHE_NAME + "]");
        join(cacheSupplier.getMarcoSourceCache().removeAll());
        join(cacheSupplier.getImageCache().removeAll());
    }
}

