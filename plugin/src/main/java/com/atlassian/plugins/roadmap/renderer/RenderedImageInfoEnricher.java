package com.atlassian.plugins.roadmap.renderer;

import com.atlassian.plugins.roadmap.models.Bar;
import com.atlassian.plugins.roadmap.models.Lane;
import com.atlassian.plugins.roadmap.models.LaneColor;
import com.atlassian.plugins.roadmap.models.Marker;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.plugins.roadmap.renderer.beans.TimelinePosition;

/**
 * Enrich the {com.atlassian.plugins.roadmap.renderer.AbstractRoadmapRenderer} when drawing the image
 */
public interface RenderedImageInfoEnricher
{
    void enrichBar(int x, int y, int width, int height, Bar task);
    void enrichBarTitle(int x, int y, int width, int height, Bar bar, LaneColor laneColor);
    void enrichLane(int x, int y, int width, int height, Lane theme);
    void enrichColumn(int x, int y, int width, int height, TimelinePosition column);
    void enrichMarker(int x1, int y1, int x2, int y2, Marker marker);
    void enrichContainer(TimelinePlanner Roadmap, int width);
    void enrichColumnText();
}
