package com.atlassian.plugins.roadmap.rest;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.plugins.roadmap.BarParam;
import com.atlassian.plugins.roadmap.PageLinkParser;
import com.atlassian.plugins.roadmap.TimelinePlannerJsonBuilder;
import com.atlassian.plugins.roadmap.TimelinePlannerMacroManager;
import com.atlassian.plugins.roadmap.TimelinePlannerMacroManager.LinkStatus;
import com.atlassian.plugins.roadmap.models.RoadmapPageLink;
import com.google.gson.JsonObject;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/")
public class TimelinePlannerResource
{
    private final TimelinePlannerMacroManager timelinePlannerMacroManager;
    private final PageLinkParser linkParser;

    public TimelinePlannerResource(TimelinePlannerMacroManager timelinePlannerMacroManager, PageLinkParser pageLinkParser)
    {
        this.timelinePlannerMacroManager = timelinePlannerMacroManager;
        this.linkParser = pageLinkParser;
    }

    /**
     * Get the status of Bar-Pagelink
     *
     * @param barId status bar id
     * @return json object
     * <pre>
     *     {@code
     *          status: {@link LinkStatus}, //normal status
     *          pageLink: {@link RoadmapPageLink} // with REDEEM status
     *     }
     * </pre>
     */
    @GET
    @Consumes({APPLICATION_JSON})
    @Produces({APPLICATION_JSON})
    @Path("bar/{barId}/status")
    public Response getBarStatus(@PathParam("barId") String barId)
    {
        LinkStatus linkStatus = timelinePlannerMacroManager.checkStatus(barId);
        JsonObject json = new JsonObject();
        json.addProperty("status", String.valueOf(linkStatus));
        if (linkStatus == LinkStatus.REDEEM)
        {
            json.addProperty("pageLink", TimelinePlannerJsonBuilder.toJson(timelinePlannerMacroManager.getBarPageLink(barId)));
            timelinePlannerMacroManager.removeStatus(barId);
            timelinePlannerMacroManager.removeBarPageLink(barId);
        }
        return Response.ok(json.toString()).build();
    }

    /**
     * Register the status for a Bar
     *
     * @param status {@link LinkStatus}
     * @param barId String represent as a Bar identifier
     * @return empty
     */
    @PUT
    @Path("bar/{barId}/{status}")
    public Response putBarStatus(@PathParam("status") String status, @PathParam("barId") String barId)
    {
        timelinePlannerMacroManager.put(barId, LinkStatus.valueOf(status));
        return Response.ok().build();
    }

    /**
     * Reset linked page of Roadmap Bar to empty
     *
     * @param context the context of Roadmap Bar
     * @return empty
     */
    @POST
    @Path("bar/pagelink/reset")
    public Response resetBarPageLink(Map context)
    {
        if (!validateBarContextMap(context))
        {
            return Response.status(Response.Status.PRECONDITION_FAILED).build();
        }

        timelinePlannerMacroManager.updatePagelinkToRoadmapBar(BarParam.fromMap(context), new RoadmapPageLink());
        return Response.ok().build();
    }

    /**
     * Link a page to Roadmap Bar
     *
     * @param context the context of Roadmap Bar and linked page ID
     * @return empty
     */
    @POST
    @Path("bar/pagelink/dolink")
    public Response linkPageToBar(Map context)
    {
        if (!validateBarContextMap(context))
        {
            return Response.status(Response.Status.PRECONDITION_FAILED).build();
        }

        String linkedPageId = (String) context.get("linkedPageId");
        timelinePlannerMacroManager.updatePagelinkToRoadmapBar(BarParam.fromMap(context), Long.valueOf(linkedPageId));
        return Response.ok().build();
    }

    /**
     * Retrieve page links from the list of  wiki links
     *
     * @param linkData data map that contains needed information for page links extraction:
     *      - wikiLinks: the wiki links of pages that linked to Roadmap Bars
     *      - roadmapSpace: key of the space that contains Roadmap instance
     * @return a Map of page links that extracted from passed in wiki links
     */
    @POST
    @Path("extractPageLinks")
    @Consumes({APPLICATION_JSON})
    @Produces({APPLICATION_JSON})
    public Response extractPageLinks(Map linkData)
    {
        String currentSpaceKey = linkData.get("roadmapSpace").toString();
        Map<String, String> wikiLinks = (Map<String, String>) linkData.get("wikiLinks");

        Map<String, RoadmapPageLink> pageLinks = new LinkedHashMap<String, RoadmapPageLink>();
        for (String key: wikiLinks.keySet())
        {
            pageLinks.put(key, linkParser.resolveConfluenceLink(wikiLinks.get(key), currentSpaceKey));
        }

        return Response.ok(pageLinks).build();
    }

    private boolean validateBarContextMap(Map contextMap)
    {
        return contextMap.containsKey("roadmapContentId") &&
                contextMap.containsKey("version") &&
                contextMap.containsKey("roadmapHash") &&
                contextMap.containsKey("roadmapBarId");
    }
}
