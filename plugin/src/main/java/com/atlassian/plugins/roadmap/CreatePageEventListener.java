package com.atlassian.plugins.roadmap;

import java.util.Map;

import com.atlassian.confluence.event.events.content.blogpost.BlogPostCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.plugins.createcontent.api.events.BlueprintPageCreateEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.plugins.roadmap.TimelinePlannerMacroManager.LinkStatus;
import com.atlassian.plugins.roadmap.models.RoadmapPageLink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//@Component(value = "createPageListener")
public class CreatePageEventListener
{
    protected static final Logger log = LoggerFactory.getLogger(CreatePageEventListener.class);
    private TimelinePlannerMacroManager timelinePlannerMacroManager;

//    @Autowired
    public CreatePageEventListener(TimelinePlannerMacroManager timelinePlannerMacroManager)
    {
        this.timelinePlannerMacroManager = timelinePlannerMacroManager;
    }

    @EventListener
    public void pageCreateEvent(PageCreateEvent event)
    {
        handleRoadmapLink(event.getPage(), (Map<String, ?>) event.getContext());
    }

    @EventListener
    public void blogpostCreateEvent(BlogPostCreateEvent event)
    {
        handleRoadmapLink(event.getBlogPost(), (Map<String, ?>) event.getContext());
    }

    @EventListener
    public void blueprintPageCreateEvent(BlueprintPageCreateEvent event)
    {
        handleRoadmapLink(event.getPage(), event.getContext());
    }

    private void handleRoadmapLink(AbstractPage linkPage, Map<String, ?> context)
    {
        /**
         * handle roadmap following:
         * viewmode: roadmapBarId + roadmapHash + id + version, need to find the bar and update on server.
         * editmode: roadmapBarId, just need to keep the status on server, updateRoadmap just a clear signal to know that don't need to update to roadmap on server side.
         */
        if (!context.containsKey("roadmapBarId") || 
                (!context.containsKey("updateRoadmap") && !(context.containsKey("roadmapHash") && context.containsKey("roadmapContentId") && context.containsKey("version")))
           )
        {
            return;
        }

        timelinePlannerMacroManager.put((String)context.get("roadmapBarId"), LinkStatus.REDEEM);
        timelinePlannerMacroManager.updatePagelinkToRoadmapBar(BarParam.fromMap(context), new RoadmapPageLink(linkPage));
    }
}
