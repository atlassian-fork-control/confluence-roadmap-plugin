package com.atlassian.plugins.roadmap.beans;

public class RoadmapTask {
    public String title, description;
    public String startid, endid;
    public Double startpos, endpos;
    public Integer row;
}
