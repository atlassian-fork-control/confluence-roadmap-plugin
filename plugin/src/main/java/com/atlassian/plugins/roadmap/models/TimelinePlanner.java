package com.atlassian.plugins.roadmap.models;

import java.util.List;

public class TimelinePlanner
{
    private String title;
    private Timeline timeline;
    private List<Lane> lanes;
    private List<Marker> markers;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Timeline getTimeline()
    {
        return timeline;
    }

    public void setTimeline(Timeline timeline)
    {
        this.timeline = timeline;
    }

    public List<Lane> getLanes()
    {
        return lanes;
    }

    public void setLanes(List<Lane> lanes)
    {
        this.lanes = lanes;
    }

    public List<Marker> getMarkers()
    {
        return markers;
    }

    public void setMarkers(List<Marker> markers)
    {
        this.markers = markers;
    }
}
