package com.atlassian.plugins.roadmap;

import org.apache.commons.lang3.ArrayUtils;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

public class FontUtils
{
    private static final int MINIMUM_CHARACTER_IN_BOX = 3;
    private static final String ELLIPSIS =  "\u2026";

    public static void main(String[] args)
    {
        final String str = "Angela versus Isabelle. Height, advantage Isabelle. Birthing hips, advantage Isabelle. Remaining childbearing years, advantage Isabelle. Legal obligation, advantage Angela.";

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(200, 200);
        frame.setVisible(true);

        frame.getContentPane().add(new JPanel()
        {

            @Override
            public void paint(Graphics g)
            {
                g.setFont(g.getFont().deriveFont(64f));
                FontMetrics fontMetrics = getFontMetrics(g.getFont());

                int width = getWidth();
                String[] lines = wrap(str, width, fontMetrics);

                int y = fontMetrics.getAscent();
                g.setColor(Color.black);
                g.fillRect(0, 0, getWidth(), getHeight());
                for (String line : lines)
                {
                    g.setColor(Color.white);
                    g.drawString(line, 0, y);
                    y += fontMetrics.getHeight();
                }
            }
        });

    }

    /**
     * Returns the separate lines of the given string after its been wrapped to fit in the width constraint.
     *
     * @param str   the string to wrap
     * @param width the max width
     * @param fm    the font metrics to use
     * @return an array of the lines
     */
    public static String[] wrap(String str, int width, FontMetrics fm)
    {
        List<String> lines = new ArrayList<String>();

        StringBuilder currentBlock = new StringBuilder();
        StringBuilder currentLine = new StringBuilder();
        for (int i = 0; i < str.length(); i++)
        {
            char c = str.charAt(i);

            int x = fm.stringWidth(currentLine.toString() + currentBlock.toString());
            currentBlock.append(c);

            if (c == ' ')
            {
                currentLine.append(currentBlock.toString());
                currentBlock.setLength(0);
            }
            else if (i == str.length() - 1 || x > width)
            {
                if (currentLine.length() == 0)
                {

                    String s = currentBlock.toString();
                    currentBlock.setLength(0);
                    while (fm.stringWidth(s) > width)
                    {
                        currentBlock.append(s.charAt(s.length() - 1));
                        s = s.substring(0, s.length() - 1);
                    }
                    lines.add(s);
                }
                else
                {
                    lines.add(currentLine.toString());
                    if (i == str.length() - 1 && currentBlock.length() > 0)
                    {
                        if (x > width)
                        {
                            lines.add(currentBlock.toString());
                        }
                        else
                        {
                            lines.remove(lines.size() - 1);
                            lines.add(currentLine.toString() + currentBlock.toString());
                        }
                    }
                    currentLine.setLength(0);
                }
            }

        }

        return lines.toArray(new String[lines.size()]);
    }

    /**
     * Returns the separate lines of the given string after its been wrapped to fit in the width constraint
     * with limited line, if total line is bigger than {@code numberOfLine} it will cut and add ellipsis to the last item
     *
     * @param str the string to wrap
     * @param width the max width
     * @param fm the font metrics to use
     * @param numberOfLine limit line
     * @return an array of the lines with {@code numberOfLine} limited.
     */
    public static String[] wrap(String str, int width, FontMetrics fm, int numberOfLine)
    {
        String[] wrapText = wrap(str, width, fm);
        if(wrapText.length <= numberOfLine)
        {
            return wrapText;
        }
        String[] wrapTextLimitLine = (String[]) ArrayUtils.subarray(wrapText, 0, numberOfLine);
        String lastStringItem = wrapTextLimitLine[numberOfLine - 1];
        wrapTextLimitLine[numberOfLine - 1] = lastStringItem.substring(0, lastStringItem.length() - 2) + ELLIPSIS;
        return wrapTextLimitLine;
    }

    /**
     * Cut string with Ellipsis if the text is too long
     * @param str input string
     * @param rc  the rectanglebox we should draw text inside
     * @param font  font to draw string
     * @param g Graphic
     * @param margin the margin value
     * @param isVertical {@code true} if we want to draw the text vertically, otherwise {@code false} for horizontal
     * @return String cutting with ellipsis
     */
    public static String cutTextInBox(String str, Rectangle rc, Font font, Graphics g, int margin, boolean isVertical)
    {
        FontMetrics fontMetrics = g.getFontMetrics(font);
        int strWidth = fontMetrics.stringWidth(str);
        int strLength = str.length();

        int lengthOfBox = isVertical ? rc.height : rc.width;
        int charPerLine = (int) (strLength * (lengthOfBox - margin * 2) / (double) strWidth);

        if (strLength <= charPerLine || strLength < MINIMUM_CHARACTER_IN_BOX)
        {
            return str;
        }
        if (charPerLine > MINIMUM_CHARACTER_IN_BOX)
        {
            return str.substring(0, charPerLine - 1) + ELLIPSIS;
        }
        return ELLIPSIS;
    }
}