package com.atlassian.plugins.roadmap.renderer;

import com.atlassian.plugins.roadmap.placeholder.PlaceholderImageFactory;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.atlassian.plugins.roadmap.FontUtils;
import com.atlassian.plugins.roadmap.models.Bar;
import com.atlassian.plugins.roadmap.models.Lane;
import com.atlassian.plugins.roadmap.models.LaneColor;
import com.atlassian.plugins.roadmap.models.Marker;
import com.atlassian.plugins.roadmap.models.Timeline;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.plugins.roadmap.renderer.beans.TimelinePosition;
import com.atlassian.plugins.roadmap.renderer.beans.TimelinePositionTitle;
import com.atlassian.plugins.roadmap.renderer.helper.TimeLineColorHelper;
import com.atlassian.plugins.roadmap.renderer.helper.TimeLineHelper;
import com.atlassian.sal.api.message.I18nResolver;

import com.google.common.collect.Sets;

/**
 * Base class for all road map renderer. The implementation is not thread-safe
 */
abstract class AbstractTimelinePlannerRenderer
{
    private I18nResolver i18n;

    private static final Font loadedFont = loadFont();

    private static Font loadFont()
    {
        return new Font("SansSerif", Font.PLAIN, 20);
    }

    private static final Color COLOR_TEXT = new Color(0x707070);
    private static final Color COLOR_BORDER = new Color(0xD1D1D1);
    private static final Color COLOR_BACK_COLUMN = new Color(0xF5F5F5);
    private static final Color COLOR_ROADMAP_BASE = new Color(0xFFFFFF);
    private static final Color COLOR_MARKER = new Color(0xD04437);

    private static final Font FONT_TITLE = loadedFont.deriveFont(Font.PLAIN, 20);
    private static final Font FONT_COLUMNS = loadedFont.deriveFont(Font.PLAIN, 13);
    private static final Font FONT_COLUMN_YEAR = loadedFont.deriveFont(Font.BOLD, 13);
    private static final Font FONT_LANES = loadedFont.deriveFont(Font.BOLD, 13);
    private static final Font FONT_BARS = loadedFont.deriveFont(Font.BOLD, 12);
    private static final Font FONT_MARKERS = loadedFont.deriveFont(Font.PLAIN, 14);

    private static final Stroke STROKE_MARKER = new BasicStroke(1.5f);
    private static final Stroke STROKE_COLUMN_LINE = new BasicStroke(0.3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1, new float[]{3.5f, 6.5f}, 0);

    protected static final int PLACEHOLDER_HEIGHT = 30;

    protected static final int ROADMAP_MONTH_COLUMN_WIDTH = 100;
    protected static final int ROADMAP_WEEK_COLUMN_WIDTH = 100;
    protected static final int MIN_LANE_HEIGHT = 97;
    private static final int MARGIN_TITLE = 20;
    private static final int MARGIN_LANE = 10;
    private static final int PADDING_LANE_TITLE = 10;
    private static final int MARGIN_TOP_COLUMNS = 5;
    private static final int MARGIN_BAR = 8;
    private static final int MARGIN_BAR_HORIZONTAL = 1;
    private static final int MARGIN_MARKER_LINE = 10;
    private static final int MARGIN_MARKER = 15;
    private static final int MARKER_TITLE_WIDTH = 100;
    private static final int MARKER_TITLE_LINE = 2;
    private static final int MARGIN_TOP = 30;

    private static final int CORNER_SIZE_LANE = 0;
    private static final int CORNER_SIZE_BAR = 4;

    private static final int SIZE_HEIGHT_COLUMN = 40;
    private static final int SIZE_HEIGHT_BAR = 37;

    private final FontMetrics fmTitle;
    private final FontMetrics fmCols;
    private final FontMetrics fmTheme;
    private final FontMetrics fmTask;
    private final FontMetrics fmMarker;

    protected abstract Graphics2D createDummyGraphics2D();

    protected abstract Graphics2D createGraphics2D(int width, int height);

    protected abstract RenderedImageInfoEnricher createEnricher();

    public void setI18n(I18nResolver i18n)
    {
        this.i18n = i18n;
    }

    protected AbstractTimelinePlannerRenderer()
    {
        Graphics2D dummyG2 = createDummyGraphics2D();
        this.fmTitle = dummyG2.getFontMetrics(FONT_TITLE);
        this.fmCols = dummyG2.getFontMetrics(FONT_COLUMNS);
        this.fmTheme = dummyG2.getFontMetrics(FONT_LANES);
        this.fmTask = dummyG2.getFontMetrics(FONT_BARS);
        this.fmMarker = dummyG2.getFontMetrics(FONT_MARKERS);
        dummyG2.dispose();
    }

    /**
     * Draw {TimelinePlanner} base on java2d api, use width and height option control size of image
     * @param timelinePlanner to render image
     * @param widthOption option width value to control width of image
     * @param heightOption option height value to control height of image
     * @param isPlaceholder true: render place holder image
     * @throws IOException when has rendering problem
     */
    protected void drawImage(TimelinePlanner timelinePlanner, Optional<Integer> widthOption, Optional<Integer> heightOption, boolean isPlaceholder) throws IOException
    {
        int wTheme = fmTheme.getHeight() + MARGIN_LANE * 2;
        int wTitle = MARGIN_TITLE; // temporary remove title
        int wTitleTheme = wTitle + wTheme;
        if (timelinePlanner.getTimeline().getDisplayOption() == Timeline.DisplayOption.MONTH)
        {
            correctTimelineBoundary(timelinePlanner.getTimeline()); //always draw timeline with startDate as the first day of month and endDate as the end of day
        }
        int wColumns = getTimelineWidth(timelinePlanner.getTimeline());
        int hRoadmap = getRoadmapHeight(timelinePlanner.getLanes());
        int wRoadmap = wColumns + wTitleTheme;

        int realHeight = hRoadmap + SIZE_HEIGHT_COLUMN + MARGIN_MARKER_LINE + MARGIN_MARKER * 2 + fmMarker.getHeight() + MARGIN_TOP + fmMarker.getHeight() * MARKER_TITLE_LINE;
        int realWidth = wRoadmap + MARKER_TITLE_WIDTH / 2;

        int finalWidth = widthOption.isPresent() ? Math.min(realWidth, widthOption.get()) : realWidth;
        int finalHeight = heightOption.isPresent() ? Math.min(realHeight, heightOption.get()) : realHeight;

        Graphics2D g2;
        if (isPlaceholder)
        {
            g2 = createGraphics2D(finalWidth, finalHeight + PLACEHOLDER_HEIGHT);
            PlaceholderImageFactory.drawPlaceholderImage(g2, loadedFont, i18n);
            g2.translate(0, PLACEHOLDER_HEIGHT); // draw Roadmap image after Placeholder image
        }
        else
        {
            g2 = createGraphics2D(finalWidth, finalHeight);
        }

        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0,  finalWidth, finalHeight); //draw white background for image

        RenderedImageInfoEnricher enricher = createEnricher();

        // Create the real image with the correct size
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        Stroke origStroke = g2.getStroke();
        AffineTransform origTransform = g2.getTransform();

        // Set drawing parameters
        DrawingParams params = new DrawingParams();
        params.enricher = enricher;
        params.fmCols = fmCols;
        params.fmMarker = fmMarker;
        params.fmTask = fmTask;
        params.fmTheme = fmTheme;
        params.fmTitle = fmTitle;
        params.hRoadmap = hRoadmap;
        params.wRoadmap = wRoadmap;
        params.origTransform = origTransform;
        params.origStroke = origStroke;
        params.realHeight = realHeight;
        params.wColumns = wColumns;
        params.wTheme = wTheme;
        params.wTitle = wTitle;
        params.wTitleTheme = wTitleTheme;

        drawTimeline(g2, timelinePlanner.getTimeline(), params);
        drawMarkers(g2, timelinePlanner, params);
        drawLanes(g2, timelinePlanner, params);
        drawTimelineBorder(g2, timelinePlanner.getTimeline(), params);
        enricher.enrichContainer(timelinePlanner, params.wRoadmap);

        if (isPlaceholder)
        {
            g2.translate(0, 0);
        }
    }

      //update timeline and change:
      //  - startDate: must be the first day of month
      //  - endDate: must be the last day of month
    private void correctTimelineBoundary(Timeline timeline)
    {
        Calendar calendarStartDate = Calendar.getInstance();
        calendarStartDate.setTime(timeline.getStartDate());
        calendarStartDate.set(Calendar.DAY_OF_MONTH, 1);
        calendarStartDate.set(Calendar.HOUR_OF_DAY, 0);
        calendarStartDate.set(Calendar.MINUTE, 0);
        calendarStartDate.set(Calendar.SECOND, 0);
        calendarStartDate.set(Calendar.MILLISECOND, 0);

        Calendar calendarEndDate = Calendar.getInstance();
        calendarEndDate.setTime(timeline.getEndDate());
        calendarEndDate.set(Calendar.DAY_OF_MONTH, calendarEndDate.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendarEndDate.set(Calendar.HOUR_OF_DAY, 23);
        calendarEndDate.set(Calendar.MINUTE, 59);
        calendarEndDate.set(Calendar.SECOND, 59);
        calendarEndDate.set(Calendar.MILLISECOND, 999);

        timeline.setStartDate(calendarStartDate.getTime());
        timeline.setEndDate(calendarEndDate.getTime());
    }

    private void drawTitle(Graphics2D g2, TimelinePlanner timelinePlanner, DrawingParams params)
    {
        g2.setColor(COLOR_ROADMAP_BASE);
        g2.fillRect(0, 0, params.wTitleTheme, params.realHeight - (MARGIN_TITLE + MARGIN_MARKER_LINE + MARGIN_MARKER + params.fmMarker.getHeight() * MARKER_TITLE_LINE + 1));//+1 to avoid overlap text 
        g2.fillRect(params.wRoadmap, 0, MARKER_TITLE_WIDTH / 2, params.realHeight - (MARGIN_TITLE + MARGIN_MARKER_LINE + MARGIN_MARKER + params.fmMarker.getHeight() * MARKER_TITLE_LINE + 1));
    }

    private void drawTimeline(Graphics2D g2, Timeline timeline, DrawingParams params)
    {
        Set<String> yearStacks = Sets.newHashSet();

        List<TimelinePosition> columns = TimeLineHelper.getColumnPosition(timeline);
        g2.setColor(COLOR_BACK_COLUMN);
        g2.setStroke(STROKE_COLUMN_LINE);

        int colXPos = params.wTitleTheme;
        int columnWidth = getColumnWidth(timeline);
        for (Iterator<TimelinePosition> timelineIt = columns.iterator(); timelineIt.hasNext();)
        {
            TimelinePosition timelinePosition = timelineIt.next();
            g2.setFont(FONT_COLUMNS);
            g2.setColor(COLOR_TEXT);
            g2.drawLine(colXPos, SIZE_HEIGHT_COLUMN + MARGIN_TOP, colXPos, SIZE_HEIGHT_COLUMN + params.hRoadmap + MARGIN_TOP);
            params.enricher.enrichColumn(colXPos, SIZE_HEIGHT_COLUMN + 1, columnWidth, params.hRoadmap - 1, timelinePosition);

            // Draw text
            TimelinePositionTitle columnTitle = TimeLineHelper.getPositionTitle(timeline, timelinePosition, i18n);
            g2.setColor(COLOR_TEXT);
            Rectangle2D boundsCol = params.fmCols.getStringBounds(columnTitle.getMonth(), g2);
            g2.drawString(columnTitle.getMonth(), colXPos + (int) (columnWidth - boundsCol.getWidth()) / 2, (int) boundsCol.getHeight() + MARGIN_TOP_COLUMNS + MARGIN_TOP);
            String year = columnTitle.getYear();
            if (!yearStacks.contains(year))
            {
                g2.setFont(FONT_COLUMN_YEAR);
                yearStacks.add(year);
                Rectangle2D yearBoundsCol = params.fmCols.getStringBounds(year, g2);
                g2.drawString(year, colXPos + (int) (columnWidth - yearBoundsCol.getWidth()) / 2, MARGIN_TOP_COLUMNS + MARGIN_TOP);
            }
            params.enricher.enrichColumnText();

            colXPos += columnWidth;
        }

        g2.setTransform(params.origTransform);
    }

    private void drawTimelineBorder(Graphics2D g2, Timeline timeline, DrawingParams params)
    {
        g2.setStroke(new BasicStroke());
        g2.setColor(COLOR_BORDER);
        g2.drawLine(params.wRoadmap, SIZE_HEIGHT_COLUMN + MARGIN_TOP, params.wRoadmap, params.hRoadmap + SIZE_HEIGHT_COLUMN + MARGIN_TOP);
    }

    private void drawMarkers(Graphics2D g2, TimelinePlanner timelinePlanner, DrawingParams params)
    {
        // Markers //
        g2.setFont(FONT_MARKERS);
        g2.setStroke(STROKE_MARKER);
        for (Marker marker : timelinePlanner.getMarkers())
        {
            TimelinePosition markerPosition = TimeLineHelper.calculateTimelinePosition(timelinePlanner.getTimeline(), marker.getMarkerDate());
            int xPos = params.wTitleTheme + getXFromColumnPosition(timelinePlanner.getTimeline(), markerPosition.getColumn(), markerPosition.getOffset());
            if (xPos < params.wTitleTheme || xPos > params.wRoadmap)
            {
                continue;
            }
            g2.setColor(COLOR_MARKER);

            // Draw line
            int hMarkerLine = params.hRoadmap + MARGIN_MARKER_LINE + SIZE_HEIGHT_COLUMN;
            g2.drawLine(xPos, SIZE_HEIGHT_COLUMN + MARGIN_TOP, xPos, hMarkerLine + MARGIN_TOP);
            params.enricher.enrichMarker(xPos, SIZE_HEIGHT_COLUMN, xPos, hMarkerLine, marker);

            // Draw text
            Rectangle2D boundsMarker = params.fmMarker.getStringBounds(marker.getTitle(), g2);
            int yPos = hMarkerLine + MARGIN_MARKER + (int) boundsMarker.getHeight();
            String[] markerTexts = FontUtils.wrap(marker.getTitle(), MARKER_TITLE_WIDTH, g2.getFontMetrics(), MARKER_TITLE_LINE);
            int markerTextHeight = g2.getFontMetrics().getHeight();
            for (String markerText : markerTexts)
            {
                Rectangle2D boundsMarkerText = params.fmMarker.getStringBounds(markerText.trim(), g2);
                g2.drawString(markerText, Math.max(0, xPos - (int) (boundsMarkerText.getWidth() / 2)), yPos += markerTextHeight);
            }
        }
        g2.setStroke(params.origStroke);
    }

    private void drawLanes(Graphics2D g2, TimelinePlanner r, DrawingParams params)
    {
        drawLaneAndBar(g2, r, params);
        drawTitle(g2, r, params);
        drawLaneTitle(g2, r, params);
        drawLaneBorder(g2, r, params);
    }

    private void drawLaneAndBar(Graphics2D g2, TimelinePlanner r, DrawingParams params)
    {
        g2.setFont(FONT_BARS);
        int laneYPos = SIZE_HEIGHT_COLUMN;
        // Draw lanes
        for (Lane lane : r.getLanes())
        {
            // Draw bars in lane
            for (Bar bar : lane.getBars())
            {
                params.themeYPos = laneYPos;
                drawBar(g2, r, bar, lane.getColor(), params);
            }

            laneYPos += getLaneHeight(lane);
        }
    }

    private void drawLaneTitle(Graphics2D g2, TimelinePlanner r, DrawingParams params)
    {
        // Draw themes title
        int laneYPos = SIZE_HEIGHT_COLUMN + MARGIN_TOP;
        for (Lane lane : r.getLanes())
        {
            Color colorLane = TimeLineColorHelper.decodeColor(lane.getColor().getLane());
            int hTheme = getLaneHeight(lane);

            int laneTitleWidth = params.wTitle - PADDING_LANE_TITLE;//padding of title, we need a space between 'lane title' and 'lane bar'
            // Draw lane box
            g2.setFont(FONT_LANES);
            g2.setColor(colorLane);
            g2.fillRoundRect(laneTitleWidth, laneYPos, params.wTheme, hTheme, CORNER_SIZE_LANE, CORNER_SIZE_LANE);
            g2.setColor(COLOR_BORDER);
            g2.drawRoundRect(laneTitleWidth, laneYPos, params.wTheme, hTheme, CORNER_SIZE_LANE, CORNER_SIZE_LANE);

            // Draw lane title
            Rectangle2D boundsTheme = params.fmTheme.getStringBounds(lane.getTitle(), g2);
            g2.setFont(FONT_LANES);
            g2.setColor(TimeLineColorHelper.decodeColor(lane.getColor().getText()));
            g2.rotate(-Math.PI / 2);
            int yPos = (int) -(hTheme + Math.min(boundsTheme.getWidth(), hTheme - MARGIN_TITLE)) / 2 - laneYPos;
            int xPos = laneTitleWidth + (int) boundsTheme.getHeight() + MARGIN_LANE - 1;

            Rectangle rectangle = new Rectangle(params.wTitle, laneYPos, params.wTheme, hTheme - MARGIN_TOP_COLUMNS);
            String drawTitle = FontUtils.cutTextInBox(lane.getTitle(), rectangle, FONT_LANES, g2, PADDING_LANE_TITLE, true);
            g2.drawString(drawTitle, yPos, xPos);
            params.enricher.enrichLane(laneTitleWidth, laneYPos, params.wTheme, hTheme, lane);
            g2.setTransform(params.origTransform);

            laneYPos += hTheme;
        }

    }

    private void drawLaneBorder (Graphics2D g2, TimelinePlanner r, DrawingParams params)
    {
        int laneYPos = SIZE_HEIGHT_COLUMN + MARGIN_TOP;
        g2.setColor(COLOR_BORDER);
        for (Lane lane : r.getLanes())
        {
            int hTheme = getLaneHeight(lane);
            g2.drawLine(params.wTitle, laneYPos, params.wTitle + getTimelineWidth(r.getTimeline()) + params.wTheme, laneYPos);
            laneYPos += hTheme;
        }
        g2.drawLine(params.wTitle, laneYPos, params.wTitle + getTimelineWidth(r.getTimeline()) + params.wTheme, laneYPos); //draw last line
    }

    private void drawBar(Graphics2D g2, TimelinePlanner r, Bar bar, LaneColor laneColor, DrawingParams params)
    {
        TimelinePosition barStartPosition = TimeLineHelper.calculateTimelinePosition(r.getTimeline(), bar.getStartDate());
        int xPos = params.wTitleTheme + getXFromColumnPosition(r.getTimeline(), barStartPosition.getColumn(), barStartPosition.getOffset()) + MARGIN_BAR_HORIZONTAL;
        int yPos = params.themeYPos + bar.getRowIndex() * SIZE_HEIGHT_BAR + (bar.getRowIndex() + 1) * MARGIN_BAR + MARGIN_TOP;
        int wTask = params.wTitleTheme + getXFromColumnPosition(r.getTimeline(), barStartPosition.getColumn(), barStartPosition.getOffset() + bar.getDuration()) - xPos - MARGIN_BAR_HORIZONTAL;

        Color colorText = TimeLineColorHelper.decodeColor(laneColor.getText());
        Color colorBar = TimeLineColorHelper.decodeColor(laneColor.getBar());
        Color colorLane = TimeLineColorHelper.decodeColor(laneColor.getLane());
        // Draw bar background
        g2.setColor(colorBar);
        g2.fillRoundRect(xPos, yPos, wTask, SIZE_HEIGHT_BAR, CORNER_SIZE_BAR, CORNER_SIZE_BAR);
        g2.setColor(colorLane);
        g2.drawRoundRect(xPos, yPos, wTask, SIZE_HEIGHT_BAR, CORNER_SIZE_BAR, CORNER_SIZE_BAR);

        params.enricher.enrichBar(xPos, yPos, wTask, SIZE_HEIGHT_BAR, bar);

        // Draw bar text
        g2.setColor(colorText);
        g2.setFont(FONT_BARS);

        Rectangle taskBoundRectangle = new Rectangle(xPos, yPos, wTask, SIZE_HEIGHT_BAR);
        String drawTitle = FontUtils.cutTextInBox(bar.getTitle(), taskBoundRectangle, FONT_BARS, g2, MARGIN_TITLE, false);
        Rectangle2D boundsTask = params.fmTask.getStringBounds(drawTitle, g2);
        g2.drawString(drawTitle, xPos + (int) (wTask - boundsTask.getWidth()) / 2, (int) (yPos + (boundsTask.getHeight() + SIZE_HEIGHT_BAR) / 2 - params.fmTask.getDescent() + 1));
        params.enricher.enrichBarTitle(xPos, yPos, wTask, SIZE_HEIGHT_BAR, bar, laneColor);
        g2.setTransform(params.origTransform);
    }

    //--------------------------------timeline PIXEL calculation--------------------------------------------

    private int getTimelineWidth(Timeline timeline)
    {
        int columnWidth = getColumnWidth(timeline);
        return columnWidth * TimeLineHelper.getNumberOfColumnInTimeline(timeline);
    }

    private int getXFromColumnPosition(Timeline timeline, Integer columnIndex, double columnOffset)
    {
        int columnWidth = getColumnWidth(timeline);
        return columnWidth * columnIndex + (int) (columnWidth * columnOffset);
    }

    private int getRoadmapHeight(List<Lane> lanes)
    {
        int height = 0;
        for (Lane lane : lanes)
        {
            height += getLaneHeight(lane);
        }
        return height;
    }

    private int getLaneHeight(Lane lane)
    {
        int nRows = 0;
        for (Bar bar : lane.getBars())
        {
            nRows = Math.max(bar.getRowIndex(), nRows);
        }
        int hTheme = (nRows + 1) * SIZE_HEIGHT_BAR + (nRows + 2) * MARGIN_BAR;
        return Math.max(hTheme, MIN_LANE_HEIGHT);
    }

    private int getColumnWidth(Timeline timeline)
    {
        return (timeline.getDisplayOption() == Timeline.DisplayOption.MONTH) ? ROADMAP_MONTH_COLUMN_WIDTH : ROADMAP_WEEK_COLUMN_WIDTH;
    }
}

class DrawingParams
{
    RenderedImageInfoEnricher enricher;
    FontMetrics fmCols;
    FontMetrics fmMarker;
    FontMetrics fmTask;
    FontMetrics fmTheme;
    FontMetrics fmTitle;
    int hRoadmap;
    int wRoadmap;
    Stroke origStroke;
    AffineTransform origTransform;
    int realHeight;
    int themeYPos;
    int wColumns;
    int wTheme;
    int wTitle;
    int wTitleTheme;
}
