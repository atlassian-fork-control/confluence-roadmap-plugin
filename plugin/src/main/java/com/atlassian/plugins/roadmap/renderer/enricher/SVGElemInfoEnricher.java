package com.atlassian.plugins.roadmap.renderer.enricher;

import com.atlassian.plugins.roadmap.models.Bar;
import com.atlassian.plugins.roadmap.models.Lane;
import com.atlassian.plugins.roadmap.models.LaneColor;
import com.atlassian.plugins.roadmap.models.Marker;
import com.atlassian.plugins.roadmap.models.TimelinePlanner;
import com.atlassian.plugins.roadmap.renderer.ConfluenceSVGGraphics2D;
import com.atlassian.plugins.roadmap.renderer.RenderedImageInfoEnricher;
import com.atlassian.plugins.roadmap.renderer.beans.TimelinePosition;
import com.google.gson.Gson;
import org.w3c.dom.Element;

import java.util.UUID;

/**
 * SVG enrich by adding some more id on task, column, lane, etc... for javascript handling on client site
 */
public class SVGElemInfoEnricher implements RenderedImageInfoEnricher
{
    private final ConfluenceSVGGraphics2D graphics2D;
    private static final Gson GSON = new Gson();
    private Element svggOverlayDiv;
    private Element barOverlayDiv;

    public SVGElemInfoEnricher(ConfluenceSVGGraphics2D graphics2D)
    {
        this.graphics2D = graphics2D;
        svggOverlayDiv = graphics2D.getDOMFactory().createElement("div");
        barOverlayDiv = graphics2D.getDOMFactory().createElement("div");
    }

    private void putData(String key, Object value)
    {
        Element elem = graphics2D.getLastAddedElement();
        elem.setAttributeNS(null, key, GSON.toJson(value).replace("\"", ""));
    }

    @Override
    public void enrichBar(int x, int y, int width, int height, Bar bar)
    {
        putData("data-roadmap-bar", bar);
    }

    @Override
    public void enrichBarTitle(int x, int y, int width, int height, Bar bar, LaneColor laneColor)
    {
        Element title = graphics2D.getDOMFactory().createElement("p");
        title.setAttribute("class", "ellipsis");
        title.setTextContent(bar.getTitle());

        Element barOverlay = graphics2D.getDOMFactory().createElement("div");
        barOverlay.setAttribute("class", "overlay-element ellipsis bar-title");
        barOverlay.setAttribute("style", String.format("left:%spx; top:%spx; width:%spx; color:%s", x, y, width, laneColor.getText()));
        barOverlay.setAttribute("data-roadmap-bar", GSON.toJson(bar));
        barOverlay.setAttribute("title", bar.getTitle());
        barOverlay.appendChild(title);
        barOverlayDiv.appendChild(barOverlay);

        Element barTitleElem = graphics2D.getLastAddedElement();
        barTitleElem.getParentNode().removeChild(barTitleElem);
    }

    @Override
    public void enrichLane(int x, int y, int width, int height, Lane theme)
    {
        Element laneOverlay = graphics2D.getDOMFactory().createElement("div");
        laneOverlay.setAttribute("class", "overlay-element ellipsis rotate-text lane-title");
        laneOverlay.setAttribute("style", String.format("left:%spx; top:%spx; width:%spx; height:%spx; color:%s; margin-top:%spx", x, y, height, width, theme.getColor().getText(), height - width));
        laneOverlay.setAttribute("title", theme.getTitle());
        laneOverlay.setTextContent(theme.getTitle());
        svggOverlayDiv.appendChild(laneOverlay);

        Element laneTitleElem = graphics2D.getLastAddedElement();
        laneTitleElem.getParentNode().removeChild(laneTitleElem);
    }

    @Override
    public void enrichColumn(int x, int y, int width, int height, TimelinePosition column)
    {
        // Do nothing
    }

    @Override
    public void enrichMarker(int x1, int y1, int x2, int y2, Marker marker)
    {
        putData("data-roadmap-marker", marker);
    }

    @Override
    public void enrichContainer(TimelinePlanner timelinePlanner, int width)
    {
        Element elem = graphics2D.getRoot();
        elem.setAttributeNS(null, "data-roadmap-id", UUID.randomUUID().toString());

        svggOverlayDiv.setAttribute("style", String.format("width:%spx",  width));
        svggOverlayDiv.setAttribute("class", "svg-overlay");

        if (barOverlayDiv.hasChildNodes())
        {
            barOverlayDiv.setAttribute("class", "svg-bar-overlay");
            svggOverlayDiv.appendChild(barOverlayDiv);
        }

        graphics2D.getRoot().appendChild(svggOverlayDiv);
    }

    @Override
    public void enrichColumnText()
    {
        putData("roadmap-column-text", "true");
    }

}
