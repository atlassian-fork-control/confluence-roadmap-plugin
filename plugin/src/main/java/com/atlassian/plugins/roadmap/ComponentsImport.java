// TODO Once https://ecosystem.atlassian.net/browse/SCANNER-17 is fixed it may be possible to use spring-scanner again
// as a provided dependency. Bundling at runtime does not work
//package com.atlassian.plugins.roadmap;
//
//import com.atlassian.cache.CacheManager;
//import com.atlassian.confluence.content.CustomContentManager;
//import com.atlassian.confluence.core.ContentEntityManager;
//import com.atlassian.confluence.internal.ContentEntityManagerInternal;
//import com.atlassian.confluence.pages.CommentManager;
//import com.atlassian.confluence.pages.PageManager;
//import com.atlassian.confluence.pages.templates.PageTemplateManager;
//import com.atlassian.confluence.search.v2.SearchManager;
//import com.atlassian.confluence.security.PermissionManager;
//import com.atlassian.confluence.spaces.SpaceManager;
//import com.atlassian.confluence.xhtml.api.XhtmlContent;
//import com.atlassian.event.api.EventPublisher;
//import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
//import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
//import com.atlassian.sal.api.message.I18nResolver;
//import com.atlassian.webresource.api.assembler.PageBuilderService;
//
//import org.springframework.transaction.PlatformTransactionManager;
//
///**
// * This class contains all components that we want to import (same as declare component-import in atlassian-plugin.xml).
// */
//@SuppressWarnings("UnusedDeclaration")
//@Scanned
//public class ComponentsImport
//{
//
//    @ComponentImport
//    PermissionManager permissionManager;
//
//    @ComponentImport
//    I18nResolver i18nResolver;
//
//    @ComponentImport
//    EventPublisher eventPublisher;
//
//    @ComponentImport("contentEntityManager")
//    ContentEntityManager contentEntityManager;
//
//    @ComponentImport
//    PageManager pageManager;
//
//    @ComponentImport
//    CacheManager cacheManager;
//
//    @ComponentImport
//    SpaceManager spaceManager;
//
//    @ComponentImport
//    XhtmlContent xhtmlContent;
//
//    @ComponentImport
//    CommentManager commentManager;
//
//    @ComponentImport("contentEntityManagerInternal")
//    ContentEntityManagerInternal contentEntityManagerInternal;
//
//    @ComponentImport
//    PageBuilderService pageBuilderService;
//
//    @ComponentImport
//    SearchManager searchManager;
//
//    @ComponentImport
//    PageTemplateManager pageTemplateManager;
//
//    @ComponentImport
//    CustomContentManager contentManager;
//
//    @ComponentImport("transactionManager")
//    PlatformTransactionManager platformTransactionManager;
//}
