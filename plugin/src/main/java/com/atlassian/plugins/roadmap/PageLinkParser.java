package com.atlassian.plugins.roadmap;

import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.BlogPostResourceIdentifier;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.PageResourceIdentifier;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.ResourceIdentifier;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.SpaceResourceIdentifier;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.plugins.roadmap.models.RoadmapPageLink;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Confluence page link parser, this is a derivative of {@link com.atlassian.confluence.content.render.xhtml.editor.macro.ConfluenceContentMacroParameterParser}
 * with enhanced space detection
 *
 * @since 12.1
 */
//@Component
public class PageLinkParser
{
    private final SpaceManager spaceManager;
    private final PageManager pageManager;

//    @Autowired
    public PageLinkParser(SpaceManager spaceManager, PageManager pageManager)
    {
        this.spaceManager = spaceManager;
        this.pageManager = pageManager;
    }

    /**
     * Parse a link into {@link ResourceIdentifier}
     *
     * @param linkText the wiki link to parse into a {@link ResourceIdentifier}
     * @param spaceKey key the space that contains wiki link
     * @return {@link ResourceIdentifier} the resource identifier found for {@code linkText}, {@code null} is no resource found
     */
    public ResourceIdentifier parse(String linkText, String spaceKey)
    {
        if (linkText == null)
        {
            return null;
        }

        String destinationTitle = linkText;

        int index = linkText.indexOf(':');
        if (index >= 0)
        {
            String linkedSpaceKey = linkText.substring(0, index);
            if (spaceManager.getSpace(linkedSpaceKey) != null)
            {
                destinationTitle = linkText.substring(index + 1);
                spaceKey = linkedSpaceKey;
            }
        }

        // blog link
        if (BlogPostResourceIdentifier.isBlogPostLink(destinationTitle))
        {
            try
            {
                return BlogPostResourceIdentifier.newInstanceFromLink(destinationTitle, spaceKey);
            }
            catch (ParseException e)
            {
                // This should not ever occur, as we match the date format with the regex before passing it to the date parser,
                // though if it does we should return null and let the link renderer component handle the failure.
                return null;
            }
        }

        // page link
        if (StringUtils.isNotBlank(destinationTitle))
            return new PageResourceIdentifier(spaceKey, destinationTitle);

        // space link
        if (StringUtils.isNotBlank(spaceKey))
            return new SpaceResourceIdentifier(spaceKey);

        // not something we recognise as a confluence-content link
        return null;
    }

    /**
     * Resolve a wiki link into {@link RoadmapPageLink}
     *
     * @param linkText the wiki link to parse into a {@link RoadmapPageLink}
     * @param currentSpaceKey key the space that contains wiki link
     * @return {@link RoadmapPageLink} the resource identifier found for {@code linkText}, {@code null} is no resource found
     */
    public RoadmapPageLink resolveConfluenceLink(String linkText, String currentSpaceKey)
    {
        linkText = StringEscapeUtils.unescapeHtml4(linkText);
        ResourceIdentifier ri = parse(linkText, currentSpaceKey);
        String contentId = "";
        String spaceKey = "";
        String pageTitle = "";
        String type = "";
        RoadmapPageLink pageLink = new RoadmapPageLink();

        if (ri instanceof PageResourceIdentifier)
        {
            type = "page";
            spaceKey = ((PageResourceIdentifier) ri).getSpaceKey();
            pageTitle = ((PageResourceIdentifier) ri).getTitle();

            Page page = pageManager.getPage(spaceKey, pageTitle);
            contentId = page != null ? String.valueOf(page.getId()) : pageLink.getId();
        }
        else if (ri instanceof BlogPostResourceIdentifier)
        {
            type = "blogpost";
            spaceKey = ((BlogPostResourceIdentifier) ri).getSpaceKey();
            pageTitle = ((BlogPostResourceIdentifier) ri).getTitle();
            Calendar postingDay = ((BlogPostResourceIdentifier) ri).getPostingDay();

            BlogPost blogPost = pageManager.getBlogPost(spaceKey, pageTitle, postingDay);
            contentId = blogPost != null ? String.valueOf(blogPost.getId()) : pageLink.getId();
        }

        pageLink.setId(contentId);
        pageLink.setSpaceKey(spaceKey);
        pageLink.setTitle(pageTitle);
        pageLink.setType(type);
        pageLink.setWikiLink(StringUtils.join(new String[] {"[", linkText, "]"}));

        return pageLink;
    }
}