package com.atlassian.plugins.roadmap.upgradetask;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.migration.ExceptionTolerantMigrator;
import com.atlassian.confluence.core.BodyContent;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.core.SaveContext;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Helper
{
    private static Logger log = LoggerFactory.getLogger(Helper.class);

    public static boolean migrate(final ContentEntityObject entity, final ExceptionTolerantMigrator migrator,
            final ContentEntityManager entityManager)
            throws CloneNotSupportedException, RuntimeException
    {
        final BodyContent bodyContent = entity.getBodyContent();
        final String originalBodyText = bodyContent == null ? "" : bodyContent.getBody();

        final ConversionContext conversionContext = new DefaultConversionContext(entity.toPageContext());
        final ExceptionTolerantMigrator.MigrationResult migrationResult = migrator.migrate(originalBodyText,
                conversionContext);

        if (migrationResult == null || !migrationResult.isMigrationPerformed())
        {
            log.debug("No migration was performed for content entity {} with title '{}'", entity.getIdAsString(),
                    entity.getTitle());
            return false;
        }

        final ContentEntityObject originalVersion = (ContentEntityObject) entity.clone();
        String contentMigrationResult = migrationResult.getContent();
        entity.setBodyAsString(contentMigrationResult);

        doMigrate(entity, originalVersion, entityManager);

        return true;
    }

    private static void doMigrate(ContentEntityObject newEntity, ContentEntityObject originalEntity,
            final ContentEntityManager entityManager)
    {
        // make sure the new version is a second later than the pre-migrated version
        // to ensure assumptions made by Page version comparators continue to be true.
        final Date originalLastModificationDate = originalEntity.getLastModificationDate();
        if (originalLastModificationDate != null)
        {
            newEntity.setLastModificationDate(new Date(originalLastModificationDate.getTime() + 1000));
        }

        // we want to create new version for upgrade task and Modifier is LastModifier of previous version
        // when create new version Confluence will get Modifier from AuthenticatedUserThreadLocal we need overwrite this user
        AuthenticatedUserThreadLocal.set(originalEntity.getLastModifier());

        newEntity.setLastModifier(originalEntity.getLastModifier());
        newEntity.setVersionComment("Upgrade Task: Roadmap's unused parameter removed.");

        // If we are saving a draft we will need to update the version of the page the
        // draft applies to (without this it would point to the wiki version which
        // would be problematic
        if (newEntity instanceof Draft)
        {
            final Draft draft = (Draft) newEntity;
            draft.setPageVersion(draft.getPageVersion() + 1);
        }

        SaveContext saveContext = new DefaultSaveContext(true, true, true);

        // Various content types like Drafts, Comments, GlobalDescription, UserStatus, etc
        // implement Versioned even though we don't actually support multiple versions
        // of these types.
        // Only save Pages and Blogs as versioned objects.
        if ((newEntity instanceof Page) || (newEntity instanceof BlogPost))
        {
            entityManager.saveContentEntity(newEntity, originalEntity, saveContext);
        }
        else
        {
            entityManager.saveContentEntity(newEntity, saveContext);
        }
    }
}
