<?xml version="1.0" encoding="UTF-8"?>
<!-- Specifying the plugin key here prevents this plugin from being transformed, however we can't use spring-scanner so we
     configured spring beans and service references manually in atlassian-plugin-components.xml -->
<atlassian-plugin key="com.atlassian.confluence.plugins.confluence-roadmap-plugin" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
    </plugin-info>

    <analytics-whitelist key="roadmap-analytics-whitelist" resource="analytics/whitelist.json"/>

    <listener name="Create Page Listener" class="com.atlassian.plugins.roadmap.CreatePageEventListener" key="createPageListener">
        <description>Provides create page event to update link for Roadmap macro</description>
    </listener>

    <!-- No component or component-imports in here, this is a transformerless plugin, put them in atlassian-plugin-components.xml
        There were problems with spring scanner and baltik, so this uses xml spring configuration rather than annotations
    -->

    <xhtml-macro name="roadmap" class="com.atlassian.plugins.roadmap.RoadmapMacro" key="roadmap-macro"
                 icon="/download/resources/com.atlassian.confluence.plugins.confluence-roadmap-plugin/images/roadmap.png">
        <description key="roadmap.macro.description" />
        <category name="visuals"/>
        <parameters>
            <parameter name="source" type="string" required="true">
                <option key="showNameInPlaceholder" value="false"/>
                <option key="showValueInPlaceholder" value="false"/>
                <default>TEST</default>
            </parameter>

            <parameter name="pagelinks" type="confluence-content" multiple="true">
                <option key="delimiter" value="~~~~~"/>
            </parameter>
            <parameter name="maplinks" type="string"></parameter>
        </parameters>
    </xhtml-macro>

    <web-resource name="Roadmap Analytics Resources" key="roadmap-analytics-resources">
        <resource type="download" name="roadmap-analytics.js" location="analytics/roadmap-analytics.js"/>
    </web-resource>

    <web-resource name="Roadmap Models" key="roadmap-models">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <resource type="download" name="models/model.js" location="/models/model.js"/>
        <resource type="download" name="models/collection.js" location="/models/collection.js"/>
    </web-resource>

    <web-resource name="Roadmap Dialog resources" key="roadmap-dialog-resources">
        <resource type="download" name="roadmap-dialog.css" location="view/roadmap-dialog.css"/>

        <resource type="download" name="roadmap-dialog.js" location="view/dialog-view.js"/>

        <dependency>com.atlassian.auiplugin:jquery-ui-keycode</dependency>
        <dependency>com.atlassian.auiplugin:dialog2</dependency>
    </web-resource>

    <web-resource name="Roadmap Bar Dialog resources" key="roadmap-bar-dialog-resources">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <transformation extension="soy">
            <transformer key="soyTransformer">
                <functions>com.atlassian.confluence.plugins.soy:soy-core-functions</functions>
            </transformer>
        </transformation>

        <resource type="download" name="bar-dialog-templates.js" location="view/roadmap-editor/bar-dialog/bar-dialog-templates.soy"/>

        <resource type="download" name="roadmap-editinplace.js" location="view/roadmap-editor/bar-dialog/roadmap-editinplace.js"/>
        <resource type="download" name="bar-dialog.js" location="view/roadmap-editor/bar-dialog/bar-dialog.js"/>
        <resource type="download" name="bar-link-new-page.js" location="view/roadmap-editor/bar-dialog/bar-link-new-page.js"/>

        <dependency>${project.groupId}.${project.artifactId}:roadmap-dialog-resources</dependency>
    </web-resource>

    <web-resource name="Roadmap Utilities resources" key="roadmap-utilities-resources">
        <resource type="download" name="roadmap-helper.js" location="utils/roadmap-helper.js"/>
        <resource type="download" name="roadmap-utilities.js" location="utils/roadmap-utilities.js"/>
    </web-resource>

    <web-resource name="Roadmap Editor Toolbar View resources" key="roadmap-editor-toolbar-view-resources">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <transformation extension="soy">
            <transformer key="soyTransformer">
                <functions>com.atlassian.confluence.plugins.soy:soy-core-functions</functions>
            </transformer>
        </transformation>

        <resource type="download" name="toolbar-templates.js" location="view/roadmap-editor/toolbar/toolbar-templates.soy"/>

        <resource type="download" name="toolbar.css" location="view/roadmap-editor/toolbar/toolbar.css"/>

        <resource type="download" name="toolbar.js" location="view/roadmap-editor/toolbar/toolbar.js"/>
    </web-resource>

    <web-resource name="Roadmap Editor Timeline View resources" key="roadmap-editor-timeline-view-resources">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <transformation extension="soy">
            <transformer key="soyTransformer">
                <functions>com.atlassian.confluence.plugins.soy:soy-core-functions</functions>
            </transformer>
        </transformation>

        <resource type="download" name="timeline-templates.js" location="view/roadmap-editor/timeline/timeline-templates.soy"/>
        <resource type="download" name="timeline.js" location="view/roadmap-editor/timeline/timeline.js"/>
        <resource type="download" name="timeline-column.js" location="view/roadmap-editor/timeline/timeline-column.js"/>
    </web-resource>

    <web-resource name="Roadmap Editor View resources" key="roadmap-editor-view-resources">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <transformation extension="soy">
            <transformer key="soyTransformer">
                <functions>com.atlassian.confluence.plugins.soy:soy-core-functions</functions>
            </transformer>
        </transformation>

        <resource type="download" name="templates.js" location="view/roadmap-editor/templates.soy"/>

        <resource type="download" name="bar.js" location="view/roadmap-editor/bar/bar.js"/>
        <resource type="download" name="bar-row.js" location="view/roadmap-editor/bar/bar-row.js"/>
        <resource type="download" name="bar-row-new.js" location="view/roadmap-editor/bar/bar-row-new.js"/>

        <resource type="download" name="lane-dialog.js" location="view/roadmap-editor/lane-dialog/lane-dialog.js"/>
        <resource type="download" name="lane-rename-dialog.js" location="view/roadmap-editor/lane-dialog/lane-rename-dialog.js"/>

        <resource type="download" name="marker-dialog.js" location="view/roadmap-editor/marker-dialog/marker-dialog.js"/>
        <resource type="download" name="marker-rename-dialog.js" location="view/roadmap-editor/marker-dialog/marker-rename-dialog.js"/>

        <resource type="download" name="lane.js" location="view/roadmap-editor/lane.js"/>
        <resource type="download" name="marker.js" location="view/roadmap-editor/marker.js"/>
        <resource type="download" name="roadmap.js" location="view/roadmap-editor/roadmap.js"/>

        <dependency>${project.groupId}.${project.artifactId}:roadmap-models</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-editor-toolbar-view-resources</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-editor-timeline-view-resources</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-dialog-resources</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-bar-dialog-resources</dependency>
        <dependency>com.atlassian.auiplugin:jquery-ui-sortable</dependency>
        <dependency>com.atlassian.auiplugin:jquery-ui-other</dependency>
        <dependency>com.atlassian.confluence.plugins.confluence-frontend:form-state-control</dependency>
    </web-resource>

    <web-resource name="Roadmap Editor resources" key="roadmap-editor-resources">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <transformation extension="soy">
            <transformer key="soyTransformer">
                <functions>com.atlassian.confluence.plugins.soy:soy-core-functions</functions>
            </transformer>
        </transformation>

        <resource type="download" name="roadmap-editor.css" location="editor/roadmap-editor.css"/>
        <resource type="download" name="datepicker-patch.css" location="editor/datepicker-patch.css"/>

        <resource type="download" name="jquery.viewport.js" location="lib/jquery.viewport.js"/>
        <resource type="download" name="jquery.ellipsis.js" location="lib/jquery.ellipsis.min.js"/>
        <resource type="download" name="md5.js" location="editor/md5.js"/>

        <resource type="download" name="roadmap-editor.js" location="editor/roadmap-editor.js"/>
        <resource type="download" name="roadmap-editor-config.js" location="editor/roadmap-editor-config.js"/>

        <dependency>com.atlassian.confluence.tinymceplugin:editor-resources</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-utilities-resources</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-editor-view-resources</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-analytics-resources</dependency>
        <context>editor-v3</context>
        <context>editor-v4</context>
    </web-resource>

    <web-resource name="Roadmap View resources" key="roadmap-view-resources">

        <resource type="download" name="roadmap-view.js" location="view/roadmap-view/roadmap-view.js"/>

        <dependency>${project.groupId}.${project.artifactId}:roadmap-bar-dialog-resources</dependency>
        <dependency>${project.groupId}.${project.artifactId}:roadmap-analytics-resources</dependency>

        <context>roadmap-view-resources</context>
    </web-resource>

    <web-resource name="placeholder-style" key="roadmap-placeholder-css">
        <resource type="download" name="roadmap-placeholder.css" location="editor/roadmap-placeholder.css"/>
        <context>editor-content</context>
    </web-resource>

    <rest key="rest" name="roadmap-api" path="/roadmap" version="1.0">
        <description>REST API for Roadmap</description>
    </rest>

    <resource type="i18n" name="i18n" location="confluence-roadmap-plugin"/>

    <resource key="icons" name="images/" type="download" location="images"/>

    <servlet name="Roadmap image servlet" i18n-name-key="roadmap.image.servlet.name" key="roadmap-image-servlet"
             class="com.atlassian.plugins.roadmap.ImageServlet">
        <description key="roadmap.image.servlet.description" />
        <url-pattern>/roadmap/image/*</url-pattern>
    </servlet>

    <capability key="roadmap-create-page-context">
        <name>roadmap-create-page-context</name>
        <url>/plugins/roadmap/create-page.action</url>
    </capability>

    <xwork key="actions" name="XWork Actions">
        <package name="createpage" extends="default" namespace="/plugins/roadmap">
            <default-interceptor-ref name="defaultStack" />
            <action name="create-page" class="com.atlassian.plugins.roadmap.CreatePageAction">
                <result name="success" type="redirectwithflash">${parentPage}</result>
            </action>
        </package>
    </xwork>


</atlassian-plugin>
