/*
 * z-index management
 * - Main popup 3004
 * - Timeline options: 4010
 * - Roadmap dialog: 4011
 * - Color picker: 4012
 * $Tips: element one should be smaller than 4010 to avoid overriding others
 */

(function() {
    window.Roadmap = window.Roadmap || {};

    /** There is three kind of color
     * lane: use this color for lane title, border of bar and background of progress
     * bar: background of bar
     * text: title of bar
     */
    Roadmap.COLORS = [
        { lane: '#f6c342', bar: '#fadb8e', text: '#594300' },
        { lane: '#3b7fc4', bar: '#6c9fd3', text: '#ffffff' },
        { lane: '#d04437', bar: '#dc7369', text: '#ffffff' },
        { lane: '#8eb021', bar: '#aac459', text: '#ffffff' },
        { lane: '#ea632b', bar: '#ef8a60', text: '#ffffff' },
        { lane: '#654982', bar: '#8c77a1', text: '#ffffff' },
        { lane: '#f15c75', bar: '#f58598', text: '#ffffff' },
        { lane: '#815b3a', bar: '#a1846b', text: '#ffffff' }
    ];

    Roadmap.TIMELINE_DISPLAY_OPTION = {
        MONTH: 'MONTH',
        WEEK: 'WEEK'
    };

    Roadmap.SHORT_DATE_FORMAT = 'YYYY-MM-DD'; //date format from aui date picker
    Roadmap.TWO_YY_SHORT_DATE_FORMAT = 'YY-MM-DD';
    Roadmap.DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

    // format in title of timeline.
    Roadmap.WEEK_FORMAT = 'DD-MMM';
    Roadmap.MONTH_FORMAT = 'MMM';

    Roadmap.TIMELINE_YEARS_LIMIT = 5;

    Roadmap.LANE_TITLE_WIDTH = 45;
    Roadmap.MARKER_HEIGHT_PADDING = 10;
    Roadmap.MARKER_TITLE_LINE = 2;
    Roadmap.LANE_PADDING = 11;
    Roadmap.BAR_PADDING = 8;
    Roadmap.BAR_MARGIN = 1;
    Roadmap.BAR_BORDER = 1;

    Roadmap.MONTH_WIDTH = 101;
    Roadmap.WEEK_WIDTH = 101;

    Roadmap.EXTRA_SPACE_AROUND = Roadmap.BAR_PADDING * 2 + Roadmap.BAR_BORDER * 2;
    Roadmap.MONTH_BAR_MIN_WIDTH = Roadmap.MONTH_WIDTH / 4 - Roadmap.EXTRA_SPACE_AROUND;
    Roadmap.WEEK_BAR_MIN_WIDTH = Roadmap.WEEK_WIDTH - Roadmap.EXTRA_SPACE_AROUND;

    Roadmap.barMinWidth = Roadmap.MONTH_BAR_MIN_WIDTH;

    /* This object is used to create an empty lane when reorder lanes, the aim is create a lane's structure only */
    Roadmap.TRANSPARENT_LANE = {
        title: '',
        color: { lane: '#ffffff', bar: '#ffffff', text: '#ffffff'}
    };

    // This is a namespace for all DragDrop configuration.
    Roadmap.DragDrop = {};

    // Config for Bar's details dialog
    Roadmap.BarDialog = {
        MAX_LENGTH_TITLE: 100,
        MAX_LENGTH_DESCRIPTION: 150
    };

    Roadmap.getDefaultRoadmapData = function() {
        // As default the duration is one year from current date
        var startDate = moment(new Date()).hours(0).minutes(0).seconds(0).milliseconds(0).toDate();
        var endDate = moment(startDate).add(11, 'months').toDate();
        return {
            title: AJS.I18n.getText('roadmap.editor.title'),
            timeline: {
                startDate: new Date(startDate),
                endDate: endDate,
                displayOption: Roadmap.TIMELINE_DISPLAY_OPTION.MONTH
            },
            lanes: [
                {
                    title: AJS.I18n.getText('roadmap.editor.default.lane1.title'),
                    color: Roadmap.COLORS[0],
                    bars: [
                        {
                            title: AJS.I18n.getText('roadmap.editor.default.bar1.title'),
                            description: AJS.I18n.getText('roadmap.editor.default.bar1.description'),
                            startDate: new Date(startDate.setDate(1)),
                            duration: 2,
                            rowIndex: 0
                        },
                        {
                            title: AJS.I18n.getText('roadmap.editor.default.bar2.title'),
                            description: AJS.I18n.getText('roadmap.editor.default.bar2.description'),
                            startDate: new Date(startDate),
                            duration: 1,
                            rowIndex: 1
                        }
                    ]
                },
                {
                    title: AJS.I18n.getText('roadmap.editor.default.lane2.title'),
                    color: Roadmap.COLORS[1],
                    bars: [
                        {
                            title: AJS.I18n.getText('roadmap.editor.default.bar3.title'),
                            description: AJS.I18n.getText('roadmap.editor.default.bar3.description'),
                            startDate: startDate,
                            duration: 2.5,
                            rowIndex: 0
                        }
                    ]
                }
            ],
            markers: [
                {
                    title: AJS.I18n.getText('roadmap.editor.default.marker1.title'),
                    markerDate: moment(startDate).date(15).toDate()
                }
            ]
        }
    };
})();

