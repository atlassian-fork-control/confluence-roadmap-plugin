(function($, _) {
    var InlineDialogView = Roadmap.InlineDialogView;
    Roadmap.LaneDialogView = InlineDialogView.extend({
        events: {
            'click #delete-button': '_onDeleteClick',
            'click #rename-button': '_onRenameClick',
            'click #add-bar-button': '_onAddBarClick'
        },

        initialize: function() {
            _.bindAll(this, '_onHide');
            this.options.dialogOptions = {
                width: this._getDialogWidth(),
                hideCallback: this._onHide
            };
            $('.roadmap-editor-popup, .roadmap-macro-view').scroll(function() {
                $('#lane-color-picker').hide();
            });

            InlineDialogView.prototype.initialize.call(this, this.options);
        },

        /**
         * Get Lane dialog content
         * @param {element} element The inline dialog element.
         * @param {function} showDialog A function that shows the inline dialog.
         */
        _getContent: function(element) {
            var laneColor = this.options.lane.model.get('color').lane;
            element.html(Confluence.Templates.Roadmap.laneDialog({
                canDelete: this.options.canDelete,
                colors: Roadmap.COLORS,
                laneColor: laneColor
            }));

            this.$el.find('.aui-button').tooltip({gravity: 's'});

            var me = this;
            var $colorItems = this.$el.find('#lane-color-picker .color-item');
            $colorItems.click(function(event) {
                event.preventDefault();
                var newColor = Roadmap.COLORS[$colorItems.index(this)];
                if (newColor.lane != laneColor) {
                    me.options.changeColor(newColor);
                }
                $('#lane-color-picker').hide();
            })
        },

        _getDialogWidth: function() {
            var iconButtonWidth = 29;
            var colorSelectWidth = 51;
            var buttonCount = 2; // currently only has 2 buttons: Add Bar & Rename lane
            if (this.options.canDelete) { // only display delete button when lane can delete
                buttonCount++;
            }
            return buttonCount * iconButtonWidth + colorSelectWidth;
        },

        _onDeleteClick: function() {
            this.options.deleteLane();
            this.remove();
        },

        _onRenameClick: function() {
            if (!this._laneRenameDialog) {
                this._laneRenameDialog = new Roadmap.LaneRenameDialogView({
                    trigger: this.options.trigger,
                    lane: this.options.lane
                });
            }
            this._laneRenameDialog.show();
            this.hide();
        },

        _onAddBarClick: function() {
            this.options.addBar();
            this.remove();
        },

        _onHide: function() {
            $('#lane-color-picker').hide();
            InlineDialogView.prototype._onHide.call(this);
        }
    });
})(AJS.$, window._);