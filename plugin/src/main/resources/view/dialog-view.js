/**
 * Currently InlineDialog doesn't support left/right orientation ( will support from version AUI 5.5.1 https://ecosystem.atlassian.net/browse/AUI-2197 )
 * SideInlineDialog has been already implemented and support left/right orientation
 * https://bitbucket.org/atlassianlabs/confluence-roadmap-plugin/src/f7c94dfb23007aa7fd0458af5e6128cdeacc6338/src/main/resources/js/SideInlineDialog.js?at=master
 * Temporary copied getDimensions & calculatePositions, getArrowPath functions to here with names_getSideDimensions & _calculateSidePositions, _getSideArrowPath
 * TODO update Roadmap InlineDialog use left/right option https://jira.atlassian.com/browse/CONFDEV-25613
 */
(function($, _) {
    window.Roadmap = window.Roadmap || {};
    Roadmap.InlineDialogView = Backbone.View.extend({
        /**
         * Initialise the view.
         *
         * @class
         * @classdesc A Backbone wrapper around `AJS.InlineDialog`.
         *
         * Subclasses must implement `_getContent()`.
         * @constructs
         * @param {object} options
         */
        initialize: function(options) {
            _.bindAll(this, '_onDocumentClick', '_onKeyDown', '_shouldHide', '_onHide');
            this._trigger = options.trigger;
            this._dialogOptions = options.dialogOptions;
            this._reset();
            var me = this;
            // hide dialog when roadmap scroll
            $('.roadmap-editor-popup, .roadmap-macro-view').scroll(function() {
                me.hide();
            });
        },

        /**
         * Determine if the inline dialog should hide in response to a click.
         *
         * Can be overridden by subclasses.
         *
         * @param {object} e A click event.
         * @returns {boolean} Whether the inline dialog should hide.
         */
        _shouldHideWhenClick: function(e) {
            return !$.contains(this._inlineDialog[0], e.target);
        },

        /**
         * Create the view's inline dialog.
         *
         * @param {trigger} the element under which the dialog is to appear.
         * @param {dialogOptions} options for inline dialog.
         * @returns {AJS.InlineDialog} The view's inline dialog.
         */
        _createInlineDialog: function(trigger, dialogOptions) {
            var inlineDialog,
                me = this;

            function getContent(element, trigger, showDialog) {
                me._getContent(element);
                showDialog();
            }

            function onBeforeShow() {
                me._onBeforeShow();

                _.delay(function() {
                    AJS.InlineDialog.current = null;
                    me._onShow();
                    $(document).on({
                        click: me._onDocumentClick,
                        keydown: me._onKeyDown
                    });
                }, me._inlineDialog.getOptions().fadeTime);
            }

            var defaultDialogOptions = {
                hideCallback: this._onHide,
                initCallback: onBeforeShow,
                noBind: true,
                persistent: true,
                preHideCallback: this._shouldHide
            };

            if (dialogOptions && dialogOptions.isSideInlineDialog) {
                _.extend(defaultDialogOptions, {
                    calculatePositions: _.compose(this._calculateSidePositions, this._getSideDimensions),
                    offsetX: 8,
                    offsetY: 0
                });

                if (Raphael.version) {
                    _.extend(defaultDialogOptions, {
                        getArrowPath: this._getSideArrowPath
                    });
                }
            }

            inlineDialog = new AJS.InlineDialog(trigger, 'roadmap-dialog', getContent,
                _.extend({}, defaultDialogOptions, dialogOptions));
            return inlineDialog;
        },

        _onHide: function() {
            $(document).off({
                click: this._onDocumentClick,
                keydown: this._onKeyDown
            });
        },

        /**
         * Remove dialog when hide it
         */
        hide: function() {
            this._inlineDialog.remove();
        },

        /**
         * Hide the inline dialog in response to a click.
         *
         * Called when the user clicks anywhere on the document.
         *
         */
        _onDocumentClick: function(e) {
            this._shouldHideWhenClick(e) && this.hide();
        },

        /**
         * Called before the inline dialog is shown.
         *
         * To be implemented by subclasses.
         */
        _onBeforeShow: $.noop,

        /**
         * Hide the inline dialog in response to pressing escape.
         *
         * @param {object} e The key down event.
         */
        _onKeyDown: function(e) {
            var isEscape = e.which === $.ui.keyCode.ESCAPE,
                isInput = $(e.target).is(":input");

            if (isEscape && !isInput) {
                this.hide();
            }
        },

        /**
         * Called after the inline dialog is shown.
         *
         * To be implemented by subclasses.
         */
        _onShow: $.noop,

        /**
         * Recreate the view's inline dialog to force content re-loading.
         *
         * This is used to work around a bug in `AJS.InlineDialog` where it
         * won't show if loading its content fails. Recreate it to reset state.
         *
         * @protected
         */
        _reset: function() {
            this._inlineDialog = this._createInlineDialog(this._trigger, this._dialogOptions);
            this.setElement(this._inlineDialog);
        },

        /**
         * To be overridden by subclasses.
         *
         * @returns {boolean} Whether the inline dialog should hide.
         */
        _shouldHide: function() {
            return true;
        },

        /**
         * Show the inline dialog.
         */
        show: function() {
            this._inlineDialog.show();
        },

        /**
         * Disable all buttons/inputs in the dialog.
         */
        disable: function() {
            this.$el.find(":input").attr("disabled", "disabled");
        },

        /**
         * Enable all buttons/inputs in the dialog.
         */
        enable: function() {
            this.$el.find(":input").removeAttr("disabled");
        },

        /**
         * Called when the dialog's form is submitted.
         *
         * To be implemented by subclasses.
         */
        _onSubmit: function() {
            throw new Error("DialogView subclasses must implement _onSubmit().");
        },

        /*
         * @param {jQuery} popup
         * @param {object} targetPosition
         * @param {object} mousePosition
         * @param {object} opts
         * @return {object}
         * @private
         */
        _getSideDimensions: function(popup, targetPosition, mousePosition, opts) {
            // Support positioning inside a scroll container other than <body>
            var constrainedScroll = opts.container.toLowerCase() !== 'body';
            var $scrollContainer = AJS.$(opts.container);
            var $scrollWindow = constrainedScroll ?
                AJS.$(opts.container).parent() :
                AJS.$(window);
            var scrollContainerOffset = constrainedScroll ?
                $scrollContainer.offset() : { left: 0, top: 0 };
            var scrollWindowOffset = constrainedScroll ?
                $scrollWindow.offset() : { left: 0, top: 0 };

            var trigger = targetPosition.target;
            var triggerOffset = trigger.offset();

            // Support SVG elements as triggers
            var triggerClientRect = trigger[0].getBoundingClientRect && trigger[0].getBoundingClientRect();

            // calculate actual width display on timeline if provide timeline width
            var width = triggerClientRect ? triggerClientRect.width : trigger.outerWidth();
            if (opts.timelineWidth) {
                var triggerPosition = trigger.position();
                if (triggerPosition.left + width > opts.timelineWidth) {
                    width = opts.timelineWidth - triggerPosition.left - 1; // 1 for timeline border
                }
            }

            var v = {
                // determines how close to the edge the dialog needs to be before it is considered offscreen
                screenPadding: 10,
                // Min distance arrow needs to be from the edge of the dialog
                arrowMargin: 5,
                window: {
                    top: scrollWindowOffset.top,
                    left: scrollWindowOffset.left,
                    scrollTop: $scrollWindow.scrollTop(),
                    scrollLeft: $scrollWindow.scrollLeft(),
                    width: $scrollWindow.width(),
                    height: $scrollWindow.height()
                },
                scrollContainer: {
                    width: $scrollContainer.width(),
                    height: $scrollContainer.height()
                },
                // Position of the trigger is relative to the scroll container
                trigger: {
                    top: triggerOffset.top - scrollContainerOffset.top,
                    left: triggerOffset.left - scrollContainerOffset.left,
                    width: width,
                    height: triggerClientRect ? triggerClientRect.height : trigger.outerHeight()
                },
                dialog: {
                    width: popup.width(),
                    height: popup.height(),
                    offset: {
                        top: opts.offsetY,
                        left: opts.offsetX
                    }
                },
                arrow: {
                    height: popup.find('.arrow').outerHeight()
                }
            };

            return v;
        },

        /**
         * @param {object} dimensions
         * @returns {object}
         * @private
         */
        _calculateSidePositions: function(dimensions) {
            var screenPadding = dimensions.screenPadding;
            var win = dimensions.window;
            var trigger = dimensions.trigger;
            var dialog = dimensions.dialog;
            var arrow = dimensions.arrow;
            var scrollContainer = dimensions.scrollContainer;

            var triggerScrollOffset = {
                top: trigger.top - win.scrollTop,
                left: trigger.left - win.scrollLeft
            };

            // Halves - because the browser doesn't do sub-pixel positioning, we need to consistently floor
            // all decimal values or you can get 1px jumps in arrow positioning when the dialog's height changes.
            var halfTriggerHeight = Math.floor(trigger.height / 2);
            var halfPopupHeight = Math.floor(dialog.height / 2);
            var halfArrowHeight = Math.floor(arrow.height / 2);

            // Figure out where to position the dialog, preferring the right
            var spaceOnLeft = triggerScrollOffset.left - dialog.offset.left - screenPadding;

            // This implementation may not be suitable for horizontally scrolling containers
            var spaceOnRight = scrollContainer.width - triggerScrollOffset.left - trigger.width - dialog.offset.left -
                screenPadding;
            var enoughSpaceOnLeft = spaceOnLeft >= dialog.width;
            var enoughSpaceOnRight = spaceOnRight >= dialog.width;
            var dialogLocation = !enoughSpaceOnRight && enoughSpaceOnLeft ? 'left' : 'right';

            // Screen padding needs to be adjusted if the arrow would extend into it
            var arrowScreenTop = triggerScrollOffset.top + halfTriggerHeight - halfArrowHeight;
            var arrowScreenBottom = win.height - arrowScreenTop - arrow.height;
            screenPadding = Math.min(screenPadding, arrowScreenTop - dimensions.arrowMargin);
            screenPadding = Math.min(screenPadding, arrowScreenBottom - dimensions.arrowMargin);

            // Figure out if the dialog needs to be adjusted up or down to fit on the screen
            var spaceAbove = Math.max(triggerScrollOffset.top - screenPadding, 0);
            var spaceBelow = Math.max(win.height - triggerScrollOffset.top - trigger.height - screenPadding, 0);
            var overflowAbove = Math.max(halfPopupHeight - halfTriggerHeight - dialog.offset.top - spaceAbove, 0);
            var overflowBelow = Math.max(halfPopupHeight - halfTriggerHeight + dialog.offset.top - spaceBelow, 0);
            var adjustmentForOverflow = overflowAbove || -overflowBelow || 0;

            // Calculate coordinates for the dialog, after adjustments for fitting onto the screen
            var popupCss = {
                top: trigger.top + halfTriggerHeight - halfPopupHeight + dialog.offset.top + adjustmentForOverflow,
                left: dialogLocation === 'right' ?
                    trigger.left + trigger.width + dialog.offset.left :
                    trigger.left - dialog.width - dialog.offset.left
            };

            var arrowGravity = 'w';
            // Calculate coordinates for the dialog's arrow. It is relative to the dialog, not the page
            var arrowCss = {
                position: 'absolute',
                top: halfPopupHeight - halfArrowHeight - adjustmentForOverflow
            };

            if (dialogLocation === 'right') {
                arrowCss.right = 'auto';
                arrowCss.left = Raphael.version ? '-7px' : '0px';
            } else {
                arrowCss.right = '-7px';
                arrowCss.left = 'auto';
                arrowGravity = 'e';
            }
            return {
                dialogLocation: dialogLocation,
                popupCss: popupCss,
                arrowCss: arrowCss,
                gravity: arrowGravity
            };
        },

        /**
         * @param {object} positions
         * @returns {string} SVG path
         * @private
         */
        _getSideArrowPath: function(positions) {
            return positions.dialogLocation === 'right' ?
                "M8,0L0,8,8,16" :
                "M8,0L16,8,8,16";
        },

        refresh: function() {
            this._inlineDialog && this._inlineDialog.refresh();
        }
    });
})(AJS.$, window._);